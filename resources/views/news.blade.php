@extends('layouts.app')


@section('content')

@php
$str = 'Таким образом новая модель организационной деятельности требуют от нас анализа позиций, занимаемых участниками в отношении поставленных задач. Равным образом начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Таким образом дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании систем массового участия.';

@endphp
<header>	
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="logo-box">
					<button class="menu-btn menu-btn-mobile"><i class="fa fa-bars"></i></button>
					@if(  !Cookie::get('light') )
					<a href="/"><img src="../img/logo.svg" alt="/"></a>
					@endif
				</div>
			</div>
			<div class="col-md-8">
				
				<div class="search-box search-box-inside">
					
					{{ Form::open(array('action' => 'HomeController@find', 'id'=>'form')) }}
					{!! csrf_field() !!}
					<div class="form-group">
						<div class="input-group">
							<select class="meds-select form-group" required multiple="multiple" name="meds[]"></select>
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit" >
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</div>
					<p class="meds-examples">Например: 
						@foreach($meds as $med)
						@if ($loop->last)
						или <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>
						@else
						<a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>,
						@endif
						@endforeach
					</p>
					{{ Form::close() }}
					
				</div>  
				
			</div>
			<div class="col-md-1 hidden-xs">
				<button class="menu-btn"><i class="fa fa-bars"></i></button>
			</div>
			<div class="absolutebutton">
				<button class="menu-btn"><i class="fa fa-bars"></i></button>
			</div>
		</div>
	</div>
</header>
    <section class="crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="/">Главная</a></li> /
                        <li class="active">Новости</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					<h1 class="title">Новости</h1>
					@foreach($news as $post)
					@php
						$date = new DateTime($post->created_at);
					@endphp
					<div class="news-item clearfix">
						<a href="#">
							<img src="/uploads/news/{{$post->logo}}" style="width: 263px; height: 151px;">
						</a>
						<div class="date date-mobile"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $date->format('d.m.Y') }}</div>
						<h3>{{ $post->title }}</h3>
						<div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $date->format('d.m.Y') }}</div>
						<p>{{ substr($post->body, 0, 828)  }}</p>
						<div class="text-right"><a href="/article/{{ $post->id }}" class="more">Подробнее</a></div>
					</div>
					@endforeach
				
					<div class="pagination">
						<ul>
							<li><a href=""><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
							<li class="active"><span>1</span></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">4</a></li>
							<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
						</ul>
					</div>
									
				</div>
				<div class="col-md-2">
					<div class="banner-block">
					</div>

				</div>
			</div>
		</div>
	</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 my-auto" style="margin-top: 10px;">
				<p>eApteka.kz 2017</p>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-8">
				<div class="f-box f-menu">
					<p>
						<ul>
							<li><a href="#">Бренды</a></li>
							<li><a href="/posts">Новости</a></li>
							<li><a href="/actions">Акции</a></li>
							<li><a href="#">Партнеры</a></li>
							<li><a href="#">Контакты</a></li>
						</ul>
					</p>
				</div>      
			</div>
		</div>
	</div>
</footer>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
	  $(".owl-carousel").owlCarousel();
	          showBanner('.banner-block');
	});
</script>
@endsection
