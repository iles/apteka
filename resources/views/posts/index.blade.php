@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1 class="title">Новости</h1>



                    @foreach($posts as $post)

                        <div class="news-item clearfix">
                            <td><img src='/uploads/logos/{{ $post->logo }}' height="100px" /></td>
                            {{--<div class="date date-mobile"><i class="fa fa-calendar" aria-hidden="true"></i> 12.10.2017</div>--}}
                            <h3>{{ $post->title }}</h3>
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $post->created_at }}</div>
                            <p>{{ $post->body }}</p>
                            <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                            <div class="text-right"><a href="post/{{ $post->id }}" class="more">Подробнее</a></div>
                        </div>
                    @endforeach
                    <div class="pagination">
                        <ul>
                            {{--<li><a href=""><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>--}}
                            {{--<li class="active"><span>{{  }}</span></li>--}}
                            <li>{{ $posts->links() }}</li>
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>--}}
                        </ul>
                    </div>

                </div>
                <div class="col-md-2">
                    <div class="banner-block">
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        showBanner('.banner-block');
    });
</script>
@endsection
