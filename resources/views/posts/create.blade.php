@extends('layouts.app')

@section('content')
    <h2>Publish a post:</h2>

    <form action="/post" method="post">

        {{csrf_field()}}
        <div class="form-group">
            <label for="title">Title:</label>
            <input class="form-control" type="text" name="title" id="title">
        </div>

        <div class="form-group">
            <label for="alias">Alias:</label>
            <input class="form-control" type="text" name="alias" id="alias">
        </div>

        <div class="form-group">
            <label for="intro">Intro:</label>
            <textarea class="form-control" type="text" name="intro" id="intro"></textarea>
        </div>

        <div class="form-group">
            <label for="intro">Body:</label>
            <textarea class="form-control" type="text" name="body" id="body"></textarea>
        </div>

        <div class="form-group">
            <label for="logo-input">Обложка</label>
            <input id="logo-input" name='user_image' type="file">
            <p class="help-block">Максимальный размер 2мб</p>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">Post</button>
        </div>

    </form>

@endsection
