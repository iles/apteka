@extends('account.admin.layout.admin')

@section('content')
<div class="row">   
    <div class="container">   

    <h2>Редактировать новость:</h2>

    <form action="/posts/{{$post->id}}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}
        {!! method_field('patch') !!}

        <div class="form-group">
            <label for="title">Заголовок:</label>
            <input class="form-control" type="text" name="title" id="title" value="{{$post->title}}">
        </div>

        <div class="form-group">
            <label for="intro">Текст:</label>
            <textarea class="form-control" type="text" name="body" rows="6" id="body">{{$post->body}}</textarea>
        </div>

        <div class="form-group">
            <label for="logo-input">Обложка</label>
            <input id="logo-input" name='user_image' type="file">
            <p class="help-block">Максимальный размер 2мб</p>
        </div>  

        <div class="form-group">
            <button class="btn btn-primary" type="submit">Обновить</button>
        </div>

    </form>
    </div>
</div>


@endsection
