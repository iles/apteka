@extends('layouts.app')

@section('content')
<div class="search-main-block text-center">
    <div class="logo-main">
        <a href="/"><img src="/img/logo.svg" alt="" width="270"></a>
    </div>
    
    <div class="auth-form">
    
    <h3>Вход в личный кабинет</h3>
    <!-- login form section-->
    <div class="auth">
        <form method="POST" action="{{ route('login') }}" class="auth"> 
            @csrf
            <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus> <br/>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                <br>
            @endif
            <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required> <br/>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <input class="btn btn-green" name="cmdweblogin" id="submit" value="Войти" type="submit">
        </form>
    </div>
    <a href="/password/reset" >Забыли пароль?</a><a href="{{ route('register') }}">Регистрация</a>

    </div>

    </div>
@endsection
