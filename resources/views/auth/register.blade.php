@extends('layouts.app')
<style>
    @media (max-width: 400px){
        ul li a{
            font-size: 10px;
        }
    }
</style>


@section('content')


    <header xmlns:line-height="http://www.w3.org/1999/xhtml">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo-box">
                        <button class="menu-btn menu-btn-mobile"><i class="fa fa-bars"></i></button>
                        <a href="/"><img src="img/logo.svg" style="right: 50px;" alt=""></a>
                    </div>
                </div>

                {{--<div class="col-md-8">--}}
                    {{--<div class="search-box search-box-inside">--}}
                        {{--<form id="form" method="post">--}}
                            {{--{!! csrf_field() !!}--}}
                            {{--<input type="text" name="search" placeholder="Введите название лекарства для поиска по аптекам Алматы" >--}}
                            {{--<button type="submit" class="btn btn-green vf-submit"><i class="fa fa-search" aria-hidden="true" style=" line-height: 1.52857143; "></i>Поиск</button>--}}
                            {{--<p class="examples">Например: <a href="#" id="zvezd">звёздочка</a>, <a href="#" id="nat">натуральные масла</a>, <a href="#" id="asper">аспирин</a> или <a href="#" id="pan">панадол</a></p>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--@section('scripts')--}}
                    {{--<script>--}}
                        {{--function request_a(id_a){--}}
                            {{--$(id_a).on('click', function () {--}}
                                {{--data = $(this).html();--}}
                                {{--$('#name').val( data );--}}
                                {{--$('#form').submit();--}}
                            {{--});--}}
                        {{--}--}}
                        {{--request_a('#zvezd');--}}
                        {{--request_a('#nat');--}}
                        {{--request_a('#asper');--}}
                        {{--request_a('#pan');--}}
                    {{--</script>--}}
                {{--@endsection--}}
                {{--<div class="col-md-1 hidden-xs">--}}
                        {{--<button class="menu-btn"><i class="fa fa-bars"></i></button>--}}
                {{--</div>--}}
            </div>
        </div>
    </header>

        <section class="crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="/">Главная</a></li> /<li class="active">Регистрация</li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                        <h1 class="title">Регистрация</h1>
                    
                     

  <!-- login form section-->
  
<form method="POST" action="{{ route('register') }}" class="user_register_form">
    {!! csrf_field() !!}
    
                <div class="form_errors"></div>
          <table class="register_table">
              
                <tr>
                    <td colspan="2"> <label for="online_radio">Интернет-аптека <input id="online_radio" type="checkbox" value="1" id=""  name="profile-is_online" style="margin: 5px;"/></label> </td>
                </tr>
              
              
                <tr>
                    <td>Логин <span class="color_red">*</span></td>
                    <td colspan="2">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Пароль <span class="color_red">*</span></td>
                    <td colspan="2">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                    </td>
                </tr>
                    <tr><td>Повтор пароля <span class="color_red">*</span></td>
                    <td colspan="2">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </td></tr>
                <tr>
                    <td>E-mail <span class="color_red">*</span></td>
                    <td colspan="2">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif        
                    </td>
                </tr>
                
              
            
                 <tr class="captcha_input_wrap">
                    <td>Код с картинки <span class="color_red">*</span></td>
                    <td colspan="2"><input class="form-control{{ $errors->has('captcha') ? ' is-invalid' : '' }}" style="display: inline-block; width: 60%" type="text" id="formcode" name="captcha" value=""/> {!! captcha_img(); !!}
                            @if ($errors->has('captcha'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('captcha') }}</strong>
                            </span>
                            @endif  

                    </td>
                </tr>
                
                 <tr>
                    <td class="align_center" colspan="2">
                    <input type="submit" name="cmdwebsignup" class="orange_btn" value="Зарегистрироваться" style="
    background-color: #198817;
    border: none;
    display: inline-block;
    padding: 7px 15px;
    border-radius: 3px;
    text-transform: uppercase;
    color: #fff;
    font-family: "SegoeUIBold";
    -webkit-transition: all .3s;
    -o-transition: all .3s;
    transition: all .3s;
"/></td>
                </tr>
        </table>
            </form>
  <script language="javascript" type="text/javascript"> 
  var id = "";
  var f = document.websignupfrm;
  var i = parseInt(id);
  if (!isNaN(i)) f.country.options[i].selected = true;
  </script>
 <script type='text/javascript'>

        if (document.websignupfrm) document.websignupfrm.username.focus();

        </script>

                    
                    
                    
                    
                    
                    
                </div>
                <div class="col-md-2">
                    <div class="banner-block">

                    </div>

                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <p>eApteka.kz 2018</p>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="f-box f-menu">
                        <p>
                            <ul>
                                <li><a href="#">Бренды</a></li>
                                <li><a href="#">Новости</a></li>
                                <li><a href="#">Акции</a></li>
                                <li><a href="#">Партнеры</a></li>
                                <li><a href="#">Контакты</a></li>
                             </ul>
                         </p>
                    </div>      
                </div>
            </div>
        </div>
    </footer>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready( function(){
        showBanner('.banner-block');
    } )
</script>
@endsection
