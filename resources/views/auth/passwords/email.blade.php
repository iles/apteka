@extends('layouts.app')

@section('content')
<div class="search-main-block text-center">
    <div class="logo-main">
        <a href="/"><img src="/img/logo.jpg" alt="" width="270"></a>
    </div>
    
    <div class="auth-form">
    
    <h3>Восстановление пароля</h3>
    <!-- login form section-->
    <div class="auth">
        <form method="POST" action="{{ route('password.email') }}" class="auth"> 
            @csrf
            <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <br/>
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                <br/>
            @endif <br/>

            <input class="btn btn-green" name="cmdweblogin" id="submit" value="Отпарвить" type="submit">
        </form>
    </div>
    <a href="/login" >Вход в личный кабинет</a><a href="{{ route('register') }}">Регистрация</a>

    </div>

    </div>
@endsection

