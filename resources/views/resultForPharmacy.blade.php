
@section('content')

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo-box">
                        <button class="menu-btn menu-btn-mobile"><i class="fa fa-bars"></i></button>
                        <a href="/"><img src="img/logo.svg" alt="/"></a>
                    </div>
                </div>
                <div class="col-md-8">

                    <div class="search-box search-box-inside">

                        {{ Form::open(array('action' => 'HomeController@find')) }}
                        {!! csrf_field() !!}
                        <input type="text" name="search" placeholder="Введите название препарата для поиска по аптекам Алматы" value="" id="getsearch"value="" id="getsearch">
                        <button class="btn btn-green"><i class="fa fa-search" aria-hidden="true" style=" line-height: 1.57857143;" ></i> Поиск</button>
                        <p>Например: <a href="#">аспирин</a>, <a href="#">звездочка</a>, <a href="#">натуральные масла</a> или <a href="#">панадол</a> </p>
                        </form>

                    </div>

                </div>
                <div class="col-md-1 hidden-xs">
                    <button class="menu-btn"><i class="fa fa-bars"></i></button>
                </div>
                <div class="absolutebutton">
                    <button class="menu-btn"><i class="fa fa-bars"></i></button>
                </div>
            </div>
        </div>
    </header>
    <section class="crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="#">Главная</a></li> /
                        <li class="active">Результаты поиска</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
                <div class="col-md-8">
                    <div class="map-block">



                        <div class="map-area" id="map"></div>

                        <div class="map-desc">
                            <ul>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Ваше местоположение</li>
                                <li><i class="fa fa-circle" aria-hidden="true"></i> Результаты</li>
                                <li><span class="area-search"></span> Область поиска</li>
                            </ul>
                        </div>
                    </div>

                    <div class="result-head row clearfix">
                        <h1 class="title">Результаты поиска</h1>

                        <div class="find-title">Вы искали <span>{{ $keyword }}</span></div>
                        <div class="find-total">Найдено: <span>{{$data->count()}} аптеки</span></div>
                        <div class="sort-box">
                            <select name="" class="form-control">
                                <option value="">По возрастанию</option>
                                <option value="">По убыванию</option>
                            </select>
                        </div>
                    </div>

                    <div class="pahrmacies">
                        @if($data->isEmpty())
                            <h3>Нет данных</h3>
                        @else
                            @foreach ($data as $pharmacy)

                                <div class="result-item row">
                                    <p class="ad-item">реклама</p>
                                    <div class="col-md-3 col-xs-3">
                                        <img src="/uploads/logos/{{ $pharmacy->logo}}" style="max-width: 100%;">
                                    </div>

                                    <p class="name"><a href="/pharmacy/{{$pharmacy->id}}">{{ $pharmacy->title }}</a>
                                    <div class="vote-box">
                                        <ul class="star">
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                        </ul>
                                    </div>

                                    </p>


                                    <div class="gray-box">
                                        <ul>
                                            <li>Открыто</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row">
                                    {{ $pharmacy->drug_title }}
                                    {{ $pharmacy->price }}
                                </div>

                            @endforeach
                        @endif
                    </div>


                </div>

                <div class="col-md-2">

                </div>
            </div>
        </div>
    </section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 my-auto" style="margin-top: 10px;">
                <p>eApteka.kz 2017</p>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="f-box f-menu">
                    <p>
                        <ul>
                            <li><a href="#">Бренды</a></li>
                            <li><a href="/posts">Новости</a></li>
                            <li><a href="/actions">Акции</a></li>
                            <li><a href="#">Партнеры</a></li>
                            <li><a href="#">Контакты</a></li>
                        </ul>
                    </p>
                </div>      
            </div>
        </div>
    </div>
</footer>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
              showBanner('.banner-block');
    });
</script>
@endsection

