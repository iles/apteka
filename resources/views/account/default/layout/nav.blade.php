      <ul class="sidebar-menu" data-widget="tree">
        <li > <a href="#"><i class="fa fa-users"></i> <span>Пользователи</span></a></li>        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-plus-circle"></i> <span>Аптеки</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Все аптеки</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Добавить</a></li>
          </ul>
        </li>        
        <li class="treeview">
          <a href="#">
            <i class="fas fa-bookmark"></i> <span>Рекламные кампании</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-plus-circle"></i> <span>Комментарии</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
      </ul>
