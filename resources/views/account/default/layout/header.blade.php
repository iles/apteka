	<section class="top-panel">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-4 col-xs-12">
				</div>
				<div class="col-md-6 col-sm-8 col-xs-12 text-right">
					<div class="auth-box">
						<ul>
							@if (Auth::check())
							<li> <a href="{{ route('/dashboard') }}"></a></li>
							<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="fa fa-exit"  aria-hidden="true"></i> Выход</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                             @csrf
                                </form>
							</li>
							@else
							<li><a href="{{ route('login') }}"><i class="fa fa-lock" aria-hidden="true"></i> Вход в личный кабинет</a></li>
							<li><a href="{{ route('register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Регистрация</a></li>		
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>