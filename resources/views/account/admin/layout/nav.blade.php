      <ul class="sidebar-menu" data-widget="tree">
        @can('user_managment')
            <li> <a href="/dashboard/users/"><i class="fa fa-users"></i> <span>Пользователи</span></a></li>
        @endcan

        
          <li class="treeview">
              <a href="#">
                <i class="fa fa-plus-circle"></i> <span>Аптеки</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>


              @role('user')
                <ul class="treeview-menu">
                    <li><a href="/admin/pharmacies/my"><i class="fa fa-circle-o"></i> Мои аптеки</a></li>
                    <li><a href="/pharm/create"><i class="fa fa-circle-o"></i> Добавить</a></li>
                </ul>
              @else
                <ul class="treeview-menu">
                  <li><a href="/admin/pharmacies"><i class="fa fa-circle-o"></i> Все аптеки</a></li>
                  <li><a href="/admin/pharmacies/new"><i class="fa fa-circle-o"></i> На модерации</a></li>
                  <li><a href="/admin/pharmacies/rejected"><i class="fa fa-circle-o"></i> Заблокированные</a></li>
                  <li><a href="/admin/pharmacies/active"><i class="fa fa-circle-o"></i> На сайте </a></li>
                  <li><a href="/pharm/create"><i class="fa fa-circle-o"></i> Добавить</a></li>
                </ul>
              @endrole
          </li> 
        
          <li class="treeview">
            <a href="#">
              <i class="fa fa-clipboard"></i><span>Рекламные кампании</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            @role('user')
            <ul class="treeview-menu">
              <li><a href="/admin/campaigns"><i class="fa fa-circle-o"></i> Мои кампании </a></li>
              <li><a href="/campaigns/create"><i class="fa fa-circle-o"></i> Добавить </a></li>
            </ul>
            @else
            <ul class="treeview-menu">
              <li><a href="/admin/campaigns"><i class="fa fa-circle-o"></i> Все кампании  </a></li>
              <li><a href="/campaigns/create"><i class="fa fa-circle-o"></i> Добавить </a></li>
            </ul>
            @endrole
          </li>           

           <li class="treeview">
            <a href="#">
              <i class="fa fa-cube"></i><span>Услуги 1с</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>            

          <li class="treeview">
            <a href="#">
              <i class="fa fa-cube"></i><span>Управление страницей</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>   


        @can('campaigns_managment')      

          <li class="treeview">
              <a href="#">
                  <i class="fas fa-bookmark"></i> <span>Новости</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="/admin/posts"><i class="fa fa-circle-o"></i> Все </a></li>
                  <li><a href="/admin/posts/create"><i class="fa fa-circle-o"></i> Добавить </a></li>
              </ul>
          </li>
       

          {{--<li class="treeview">--}}
            {{--<a href="#">--}}
              {{--<i class="fas fa-bookmark"></i> <span>Рекламные баннеры</span>--}}
              {{--<span class="pull-right-container">--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
              {{--</span>--}}
            {{--</a>--}}

            {{--<ul class="treeview-menu">--}}
              {{--<li><a href="/admin/banners"><i class="fa fa-circle-o"></i> Все </a></li>--}}
              {{--<li><a href="/banners/create/{{ $action->id  }}"><i class="fa fa-circle-o"></i> Добавить </a></li>--}}
            {{--</ul>--}}
          {{--</li>--}}



        @endcan   

        @can('actions_managment')  
        <li class="treeview">
          <a href="#">
            <i class="fa fa-plus-circle"></i> <span>Акции</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/actions"><i class="fa fa-circle-o"></i> Все</a></li>
            <li><a href="/actions/create"><i class="fa fa-circle-o"></i> Добавить</a></li>
          </ul>
        </li>
        @endcan
      </ul>

