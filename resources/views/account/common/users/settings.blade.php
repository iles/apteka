@extends('account.admin.layout.admin')

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Настройки пользователя</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::model($user, ['route' => ['user.store', $user->id]]) }}

              <div class="box-body">

 @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
@endif

                <div class="form-group">
                  {{ Form::label('firstname', 'Имя') }}
                  {{ Form::text('name', $user->name, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                  {{ Form::label('lastname', 'Email') }}
                  {{ Form::text('email', $user->email, ['class' => 'form-control']) }}
                </div>                

                <div class="form-group">
                  {{ Form::label('lastname', 'Поменять пароль') }}
                    <br/>
                    <br/>

                    <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                      <label for="new-password" class="col-md-2 control-label">Текущий пароль</label>
                      <div class="col-md-3">
                        <input id="current-password" type="password" class="form-control" name="current-password" >
                          @if ($errors->has('current-password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('current-password') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                      <label for="new-password" class="col-md-2 control-label">Новый пароль</label>
                      <div class="col-md-3">
                        <input id="new-password" type="password" class="form-control" name="new-password" >
                        @if ($errors->has('new-password'))
                        <span class="help-block">
                          <strong>{{ $errors->first('new-password') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>
                    <br/>
                    <br/>

                      <div class="form-group">
                        <label for="new-password-confirm" class="col-md-2 control-label">Подтверждение нового пароля</label>
                        <div class="col-md-3">
                          <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" >
                        </div>
                      </div>

                </div>



























              </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>

    {{ Form::close() }}
        <!-- /.box-header -->
        <!-- form start -->
         {{--{{ Form::model($user, array('route' => array('user.update', $user->id))) }}--}}
        {{--<form action='/user/update/' method="post">--}}
            {{--{{ csrf_field() }}--}}
            {{--<div class="box-body">--}}
                {{--<div class="form-group">--}}
                    {{--<label for="firstname-input">Имя</label>--}}
                    {{--<input class="form-control" id="title-input" placeholder="Имя" name="firstname" type="text" value="{{$user->firstname }}">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="lastname-input">Фамилия</label>--}}
                    {{--<input class="form-control" id="info-input" placeholder="Фамилия" name="lastname" value="{{ $user->lastname }}" type="text">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="patronic-input">Отчество</label>--}}
                    {{--<input class="form-control" id="title-input" placeholder="Имя" name="patronic" type="text" value="{{$user->patronic }}">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="lastname-input">Информация</label>--}}
                    {{--<input class="form-control" id="info-input" placeholder="Информация" name="info" value="{{ $user->info }}" type="text">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="phone-input">Телефон</label>--}}
                    {{--<input class="form-control" id="phone-input" placeholder="Телефон" name="phone" value="{{ $user->phone }}" type="text">--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--<label for="logo-input">Фотография профиля</label>--}}
                    {{--<input id="logo-input" name='user_image' type="file">--}}
                    {{--<p class="help-block">Максимальный размер 2мб</p>--}}
                {{--</div>--}}

                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input type="checkbox" name="is_online" value="1"> Аптека--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="box-footer">--}}
                {{--<button type="submit" class="btn btn-primary">Добавить</button>--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--{{ Form::open(array('action' => 'PharmacyController@store', 'files'=>true)) }}--}}

    {{--<div class="box-body">--}}
        {{--<div class="form-group">--}}
            {{--<label for="title-input">Название</label>--}}
            {{--<input class="form-control" id="title-input" placeholder="Название" name="title" type="text">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="info-input">Инфо</label>--}}
            {{--<input class="form-control" id="info-input" placeholder="Инфо" name="info" type="text">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="logo-input">Лого</label>--}}
            {{--<input id="logo-input" name='user_image' type="file">--}}
            {{--<p class="help-block">Максимальный размер 2мб</p>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="coordinates_input">Координаты</label>--}}
            {{--<input class="form-control" id="coordinates_input" name="coordinates" placeholder="Координаты" type="text">--}}
            {{--<div class="map-block" style="width: 100%; height: 300px">--}}
                {{--<div class="map-area" id="map"  style="width: 100%; height: 100%"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="checkbox">--}}
            {{--<label>--}}
                {{--<input type="checkbox" name="is_online" value="1"> Онлайн Аптека--}}
            {{--</label>--}}
        {{--</div>--}}
    {{--</div>--}}


</div>
          </div>
@endsection

