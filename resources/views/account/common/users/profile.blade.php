@extends('account.admin.layout.admin')

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Профиль пользователя</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::model($profile, ['route' => ['profile.store', $profile->id]]) }}

              <div class="box-body">

              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif

                <div class="form-group">
                  {{ Form::label('firstname', 'Имя') }}
                  {{ Form::text('firstname', $profile->firstname, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                  {{ Form::label('lastname', 'Фамилия') }}
                  {{ Form::text('lastname', $profile->lastname, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                  {{ Form::label('patronic', 'Информация о вас') }}
                  {{ Form::text('patronic', $profile->info, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                  {{ Form::label('patronic', 'Телефон') }}
                  {{ Form::text('patronic', $profile->phone, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                  <label for="logo-input">Изображение</label>
                  <input id="logo-input" name='logo' type="file">
                  <p class="help-block">Максимальный размер 2мб</p>
                </div>
              </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>

    {{ Form::close() }}
        <!-- /.box-header -->
        <!-- form start -->
         {{--{{ Form::model($user, array('route' => array('user.update', $user->id))) }}--}}
        {{--<form action='/profile/update/' method="post">--}}
            {{--{{ csrf_field() }}--}}
            {{--<div class="box-body">--}}
                {{--<div class="form-group">--}}
                    {{--<label for="firstname-input">Имя</label>--}}
                    {{--<input class="form-control" id="title-input" placeholder="Имя" name="firstname" type="text" value="{{$profile->firstname }}">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="lastname-input">Фамилия</label>--}}
                    {{--<input class="form-control" id="info-input" placeholder="Фамилия" name="lastname" value="{{ $profile->lastname }}" type="text">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="patronic-input">Отчество</label>--}}
                    {{--<input class="form-control" id="title-input" placeholder="Имя" name="patronic" type="text" value="{{$profile->patronic }}">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="lastname-input">Информация</label>--}}
                    {{--<input class="form-control" id="info-input" placeholder="Информация" name="info" value="{{ $profile->info }}" type="text">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="phone-input">Телефон</label>--}}
                    {{--<input class="form-control" id="phone-input" placeholder="Телефон" name="phone" value="{{ $profile->phone }}" type="text">--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--<label for="logo-input">Фотография профиля</label>--}}
                    {{--<input id="logo-input" name='user_image' type="file">--}}
                    {{--<p class="help-block">Максимальный размер 2мб</p>--}}
                {{--</div>--}}

                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input type="checkbox" name="is_online" value="1"> Аптека--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="box-footer">--}}
                {{--<button type="submit" class="btn btn-primary">Добавить</button>--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--{{ Form::open(array('action' => 'PharmacyController@store', 'files'=>true)) }}--}}

    {{--<div class="box-body">--}}
        {{--<div class="form-group">--}}
            {{--<label for="title-input">Название</label>--}}
            {{--<input class="form-control" id="title-input" placeholder="Название" name="title" type="text">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="info-input">Инфо</label>--}}
            {{--<input class="form-control" id="info-input" placeholder="Инфо" name="info" type="text">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="logo-input">Лого</label>--}}
            {{--<input id="logo-input" name='user_image' type="file">--}}
            {{--<p class="help-block">Максимальный размер 2мб</p>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="coordinates_input">Координаты</label>--}}
            {{--<input class="form-control" id="coordinates_input" name="coordinates" placeholder="Координаты" type="text">--}}
            {{--<div class="map-block" style="width: 100%; height: 300px">--}}
                {{--<div class="map-area" id="map"  style="width: 100%; height: 100%"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="checkbox">--}}
            {{--<label>--}}
                {{--<input type="checkbox" name="is_online" value="1"> Онлайн Аптека--}}
            {{--</label>--}}
        {{--</div>--}}
    {{--</div>--}}


</div>
          </div>
@endsection

