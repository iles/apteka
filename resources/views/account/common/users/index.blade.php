@extends('account.admin.layout.admin')
<head>
    <style>
        @media (max-width: 430px){
            #example2{
                font-size: 10px;
                /*padding: 0px;*/

            }
            #example2 td{
                padding-left: 0px;
                padding-right: 0px;
            }


        }
    </style>
</head>
@section('content')
    <section class="content-header">
      <h1>
        Пользователи
      </h1>

    </section>

    <section class="content">


      <!-- Default box -->
      <div class="box">
       <div class="box-body">

          <table id="example2" class="table table-bordered table-hover">
              <div class="col-sm-12">
                  <thead>
                  <tr>
                      <th>id</th>
                      <th>Имя</th>
                      <th>Email</th>
                      <th>Cоздан</th>
                  </tr>
                  </thead>
              </div>

              <div class="col-sm-12">
                <tbody>
                  @foreach ($users as $user)
                    <tr>
                      <td>{{ $user->id }}</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->created_at }}</td>
                    </tr>
                  @endforeach
                </tbody>
              </div>
          </table>
          </div>
        </div>
        <!-- /.box-body -->
      <!-- /.box -->




    </section>
@endsection
