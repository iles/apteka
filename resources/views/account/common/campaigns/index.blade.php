@extends('account.admin.layout.admin')
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
    <style>
        @media (max-width: 700px) {
            #example2 {
                font-size: 10px;
            }

            #example2 td a {
                font-size: 10px;
            }

            img {
                height: 50px;
            }
        }

        @media (max-width: 507px) {
            #example2 .btn {
                font-size: 7px;
                padding: 1px 1px;
            }
            #example2 td a {
                font-size: 7px;
            }

            #example2 {
                font-size: 7px;
            }
            img {
                height: 30px;
            }
        }
    </style>
</head>
@section('content')

    <section class="content-header">
      <h1>
        Рекламные кампании
        <small> <a href="/campaigns/create"><i class="fa fa-plus"></i> Добавить</a></small>
      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Главная</a></li>--}}
        {{--<li class="active"><a href="#">Все аптеки</a></li>--}}
      {{--</ol>--}}
    </section>

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            @if($campaigns->isEmpty())
              <h3>Нет данных</h3>
            @else
            {{--<div class="box-header">--}}
              {{--<h3 class="box-title">Hover Data Table</h3>--}}
            {{--</div>--}}
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                  @foreach ($campaigns as $action)
                <thead>
                  <tr>
                    <th>Превью кампании</th>
                    <th>Наименование кампании</th>
                    <th>Тип кампании</th>
                    <th>Дата начала</th>
                    <th>Дата окончания</th>
                    <th>Бюджет</th>
                    <th>Остаток средств</th>
                    <th>Статус оплаты</th>
                    <th>Количество показов</th>
                    <th>Количество кликов</th>
                    <th>Статус</th>
                      @can('pharmacy_control')
                          <th>Действие</th>
                      @endcan
                      @if($action->user_id == Auth::user()->id)
                          <th>Действие</th>
                      @endif

                  </tr>
                </thead>
                <tbody>

                  <tr>
                    <td><img src='/uploads/campaigns/{{ $action->preview }}' height="100px" /></td>
                    <td>{{ $action->title }}</td> 
                    <td>{{ $action->type }}</td> 
                    <td>{{ $action->date_start }}</td> 
                    <td>{{ $action->date_end }}</td> 
                    <td>{{ $action->budget }}</td> 
                    <td>{{ $action->rest}}</td> 
                    <td>{{ $action->payment_status}}</td> 
                    <td>{{ $action->views_amount }}</td> 
                    <td>{{ $action->click_amount }}</td> 
                    <td>{{ $action->status_id }}</td>

                      @can('pharmacy_control')
                          @if($action->status_id == 0 or $action->status_id == 2 )
                              <td>
                                  <a href="{{ action('CampaignController@aprove', $action->id) }}">
                                      <i class="fa fa-play"></i> Play
                                  </a><br>
                                  <a href="/banners/create/{{ $action->id }}">Добавить баннеры</a>
                              </td>
                          @elseif($action->status_id == 1)
                              <td>
                                  <a href="{{ action('CampaignController@aprove', $action->id) }}">
                                      <i class="fa fa-pause"></i> Pause
                                  </a><br>
                                  <a href="/banners/create/{{ $action->id }}">Добавить баннеры</a>
                              </td>
                          @endif
                      {{--<a href="/banners/create/{{ $action->id }}">Добавить баннеры</a>--}}
                      @endcan

                      {{--@if($action->user_id == Auth::user()->id)--}}
                          {{--@if($action->status_id == 2 )--}}
                              {{--<td>--}}
                                  {{--<a href="{{ action('CampaignController@aprove', $action->id) }}">--}}
                                      {{--<i class="fa fa-play"></i> Play--}}
                                  {{--</a><br>--}}
                              {{--</td>--}}
                          {{--@elseif($action->status_id == 1)--}}
                              {{--<td>--}}
                                  {{--<a href="{{ action('CampaignController@aprove', $action->id) }}"><i class="fa fa-pause"></i> Pause</a><br>--}}
                              {{--</td>--}}
                          {{--@endif--}}

                          @if($action->user_id == Auth::id() )
                              <td>
                                <a href="/banners/create/{{ $action->id }}">Добавить баннеры</a>
                              </td>
                          @endif

                      {{--@endif--}}
                  </tr>

                  @endforeach
                </tbody>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection
