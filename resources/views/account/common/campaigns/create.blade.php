@extends('account.admin.layout.admin')

@section('header-scripts')
  <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">  
@endsection



@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Добавить рекламную кампанию</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <div class="box-body">
  {{ Form::model($campaign, array('route' => 'campaign.store', $campaign->id, 'files' => true)) }}
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

      @if($pharmacies)
          <h1>Нет аптек которые можно рекламировать</h1>
      @else
          <div>
              <h2>Выберите аптеку которую будете рекламировать</h2>
              <select name="pharmacy_id">
                  @foreach($pharmacies as $pharmacy)
                      <option value="{{ $pharmacy->id }}">
                          {{ $pharmacy->title }}
                          {{--<input name="pharmacy_id" hidden value="{{ $pharmacy->id }}">--}}
                      </option>
                  @endforeach
              </select>
          </div>

              <div class="form-group">
      {{ Form::label($campaign->title, 'Наименование кампании') }}
      {{ Form::text('title', '', ['class' => 'form-control']) }}
    </div>      



    <div class="form-group">
      {{ Form::label('type', 'Тип кампании') }}
      {{ Form::select('type', ['1' => 'Баннерная реклама', '2' => 'Строчная реклама'], '1', ['class' => 'form-control'] )  }}
    </div>
    
    <div class="form-group">
      {{ Form::label($campaign->title, 'Бюджет') }}
      {{ Form::text('budget', '', ['class' => 'form-control']) }}
    </div>    

    <div class="form-group">
      {{ Form::label('date_start', 'Дата начала') }}
      {{ Form::text('date_start', $campaign->date_start, ["placeholder" => "2014-09-09 12:00:00", 'class' => 'form-control', 'id'=>'calendar1']) }}
    </div>    

    <div class="form-group">
      {{ Form::label('date_end', 'Дата окончания') }}
      {{ Form::text('date_end', $campaign->date_end, ["placeholder" => "2014-09-09 12:00:00", 'class' => 'form-control', 'id'=>'calendar2']) }}
    </div>    

    <div class="form-group">
      {{ Form::label('preview', 'Превью') }}
      {{ Form::file('preview') }}
    </div>
      <input name="user_id" hidden value="{{ Auth::user()->id }}">

    <div class="box-footer">
      {{ Form::submit('Добавить', ['class' => 'btn btn-primary']) }}
    </div>

    
      @endif

   
  {{ Form::close() }}
  </div>
</div>
@endsection

@section('scripts-files')
  <script src="/js/moment.js" type="text/javascript"></script>
  <script src="/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      $('#calendar1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
      });
      
      $('#calendar2').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
      });
    })
  </script>
@endsection
