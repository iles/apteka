@extends('account.admin.layout.admin')
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
    <style>
        @media (max-width: 700px) {
            #example2 {
                font-size: 10px;
            }

            #example2 td a {
                font-size: 10px;
            }

            img {
                height: 50px;
            }
        }

        @media (max-width: 507px) {
            #example2 .btn {
                font-size: 7px;
                padding: 1px 1px;
            }
            #example2 td a {
                font-size: 7px;
            }

            #example2 {
                font-size: 7px;
            }
            img {
                height: 30px;
            }
        }
    </style>
</head>
@section('content')
    <section class="content-header">
        <h1>
        Новости
        {{--<small> <a href="/pharmacy/create/c"><i class="fas fa-plus"></i> Добавить</a></small>--}}

        <small> <a href="/admin/posts/create"><i class="fa fa-plus"></i> Добавить</a></small>

      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Главная</a></li>--}}
        {{--<li class="active"><a href="#">Все аптеки</a></li>--}}
      {{--</ol>--}}
    </section>

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            @if($news->isEmpty())
              <h3>Нет данных</h3>
            @else

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Заголовок</th>
                  <th>Текст</th>
             <th>Лого</th>

                  @can('pharmacy_control')
                    <th>Действия</th>
                  @endcan
                </tr>
                </thead>
                <tbody>
                  @foreach ($news as $post)
                <tr>
                  <td>{{ $post->title }}</td> 
                  <td>{{ $post->body }}</td> 
              <td><img src='/uploads/news/{{ $post->logo }}' height="100px" /></td> 

                  <td>

                    <a href="{{ action('PostController@edit', $post->id) }}" ><span class="glyphicon glyphicon-pencil"></span></a>
                      <form action="/admin/post/destroy/{{ $post->id }}" method="POST" onSubmit="if(!confirm('Вы уверены?')){return false;}">
                          @csrf
                          <input value="{{ $post->id }}" hidden name="id">
                          <button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                      </form>
                    {{--<a href="{{ action('PharmacyController@destroy', $post->id) }}" class="btn btn-warning">Удалить</a>--}}
                  </td> 
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection
