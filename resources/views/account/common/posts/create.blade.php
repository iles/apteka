@extends('account.admin.layout.admin')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Добавить новость</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {{ Form::open(array('action'=>'PostController@store', 'files'=>true)) }}


            {{csrf_field()}}
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input class="form-control" type="text" name="title" id="title">
            </div>

            <div class="form-group">
                <label for="intro">Текст</label>
                <textarea class="form-control" type="text" name="body" id="body"></textarea>
            </div>

            <div class="form-group">
                <label for="logo-input">Лого</label>
                <input id="logo-input" name='logo' type="file">
                <p class="help-block">Максимальный размер 2мб</p>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>

        </form>
    </div>

    </section>
@endsection
