@extends('account.admin.layout.admin')
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
    <style>
        @media (max-width: 700px) {
            #example2 {
                font-size: 10px;
            }

            #example2 td a {
                font-size: 10px;
            }

            img {
                height: 50px;
            }
        }

        @media (max-width: 507px) {
            #example2 .btn {
                font-size: 7px;
                padding: 1px 1px;
            }
            #example2 td a {
                font-size: 7px;
            }

            #example2 {
                font-size: 7px;
            }
            img {
                height: 30px;
            }
        }
    </style>
</head>
@section('content')
    <section class="content-header">
      <h1>
        Рекламные баннеры
        <small> <a href="/banners/create"><i class="fa fa-plus"></i> Добавить</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active"><a href="#">Все аптеки</a></li>
      </ol>
    </section>

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            @if($banners->isEmpty())
              <h3>Нет данных</h3>
            @else
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Превью</th>
                    <th>Подпись</th>
                    <th>Текст</th>
                    <th>Кампания</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($banners as $action)
                  <tr>
                    <td><img src='/uploads/banners/{{ $action->image }}' height="100px" /></td>
                    <td>{{ $action->title }}</td> 
                    <td>{{ $action->text }}</td> 
                    <td>{{ $action->campaign_id }}</td> 
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection
