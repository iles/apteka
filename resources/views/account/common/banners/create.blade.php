@extends('account.admin.layout.admin')

@section('header-scripts')
  <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">  
@endsection



@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Добавить рекламный баннер</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <div class="box-body">
  {{ Form::model($banner, array('route' => 'banner.store', $banner->id, 'files' => true)) }}    

    @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all()  as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
    @endif

    <div class="form-group">
      {{ Form::label($banner->title, 'Название') }}
      {{ Form::text('title', '', ['class' => 'form-control']) }}
      <input type="text" name="campaign_id" value="{{ $id }}" hidden="true" />
        {{--<input type="text" name="pharmacy_id" value="{{ $campaign->pharmacy_id }}" hidden="true" />--}}
    </div>      


    <div class="form-group">
      {{ Form::label($banner->text, 'Текст') }}
      {{ Form::textarea('text', '', ['class' => 'form-control']) }}
    </div>    

 

    <div class="form-group">
      {{ Form::label('image', 'Изображеине (165 x 200)') }}
      {{ Form::file('image') }}
    </div>


    <div class="box-footer">
      {{ Form::submit('Добавить', ['class' => 'btn btn-primary']) }}
    </div>   
  {{ Form::close() }}
  </div>
</div>
@endsection

@section('scripts-files')
  <script src="/js/moment.js" type="text/javascript"></script>
  <script src="/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
       $('#calendar1').datetimepicker();
       $('#calendar2').datetimepicker();
    })
  </script>
@endsection
