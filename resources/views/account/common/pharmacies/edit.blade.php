@extends('account.admin.layout.admin')

@section('content')
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить аптеку</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action='/pharmacy/update/{{ $pharmacy->id }}' enctype="multipart/form-data" method="POST">
                @csrf
                <input name="id" value="{{ $pharmacy->id }}" hidden>

                            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif


              <div class="box-body">
                <div class="form-group">
                  <label for="title-input">Название</label>
                  <input class="form-control" id="title-input" placeholder="Название" name="title" type="text" value="{{ $pharmacy->title }}">
                </div>
                <div class="form-group">
                  <label for="info-input">Инфо</label>
                  <input class="form-control" id="info-input" placeholder="Инфо" name="info" value="{{ $pharmacy->info }}" type="text">
                </div>
                  <div class="form-group">
                      <label for="address-input">Адрес</label>
                      <input class="form-control" id="address-input" placeholder="Адрес" name="address" value="{{ $pharmacy->address }}" type="text">
                  </div>
                  <div class="form-group">
                      <label for="phone-input">Телефон</label>
                      <input class="form-control" id="phone-input" placeholder="Телефон" name="phone" value="{{ $pharmacy->phone }}" type="text">
                  </div>
                  <div class="form-group">
                      <label for="time_work-input">Время работы</label>
                      <input class="form-control" id="work_time-input" placeholder="Время работы" name="time_work" value="{{ $pharmacy->time_work }}" type="text">
                  </div>
                  <div class="form-group">
                      <label for="logo-input">Лого</label>
                      <img src="/uploads/logos/{{ $pharmacy->logo }}" style="width: 100px;">
                      <input id="logo-input" name='user_image' type="file">
                      <p class="help-block">Максимальный размер 2мб</p>
                  </div>
                <div class="form-group">
                  <label for="coordinates_input">Координаты</label>
                  <input class="form-control" id="coordinates_input" name="coordinates" placeholder="Координаты" type="text" value="{{ $pharmacy->coordinates }}">
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_online" > требуемый размер 200 х 278 px
                  </label>
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            </form>
          </div>


@endsection
