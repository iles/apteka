@extends('account.admin.layout.admin')

@section('header-scripts')
 <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
@endsection

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить аптеку</h3>
            </div>


            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            
            {{ Form::open(array('action'=>'PharmacyController@store', 'files'=>true)) }}

              <div class="box-body">
                <div class="form-group">
                  <label for="title-input">Название</label>
                  <input class="form-control" id="title-input" placeholder="Название" required="required" name="title" type="text">
                </div>
                <div class="form-group">
                  <label for="info-input">Инфо</label>
                  <input class="form-control" id="info-input" placeholder="Инфо"  required="required" name="info" type="text">
                </div>
                  <div class="form-group">
                      <label for="address-input">Адрес</label>
                      <input class="form-control" id="address-input" placeholder="Адрес"  required="required" name="address" type="text">
                  </div>
                  <div class="form-group row">
                      <div class="col-md-2">
                        <label for="phone-input">Телефон</label>
                        <input class="form-control" id="phone-input" placeholder="Телефон"  required="required" name="phone[]" type="text">                        
                      </div>
                      <div class="col-md-3">
                        <a href="#" id="addPhone">Добавить</a>
                      </div>
                  </div>                  

                  <div class="form-group row extra-phones">

                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-1">
                        <label for="time_work-input">Время работы</label>                        
                      </div>                 
                      
                      <div class='col-md-1'>
                        <div class="form-group">
                            <label>C</label>
                          <div class='input-group date' id='datetimepicker3'>
                            <input class="form-control" id="work_time-input" placeholder="Время работы"  required="required" name="time_work" type="text">   
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-time"></span>
                            </span>
                          </div>
                        </div>
                      </div>                       

                      <div class='col-md-1'>
                        <div class="form-group">
                            <label>По</label>
                          <div class='input-group date' id='datetimepicker4'>
                            <input class="form-control" id="work_time-to_input" placeholder="Время работы"  required="required" name="time_work2" type="text">   
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-time"></span>
                            </span>
                          </div>
                        </div>
                      </div>                       

                      <div class='col-md-1'>
                        <div class="form-group">
                          <label for="allday">Круглосуточно</label>
                          <input type="checkbox" name="allday" id="allday">
                        </div>
                      </div>                    
                    
                    </div>
                  </div>
                <div class="form-group">
                  <label for="logo-input">Лого</label>
                  <input id="logo-input" name='user_image' type="file">
                  <p class="help-block">требуемый размер 200 х 278 px</p>
                </div>
                <div class="form-group">
                  <label for="coordinates_input">Координаты</label>
                  <input class="form-control" id="coordinates_input" name="coordinates"  required="required"placeholder="Координаты" type="text">
                  <div class="map-block" style="width: 100%; height: 300px">
                    <div class="map-area" id="map"  style="width: 100%; height: 100%"></div>
                  </div>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_online" value="1"> Онлайн Аптека
                  </label>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            </form>
          </div>


    </section>
@endsection

@section('scripts')
<script>

$(document).ready(function(){
  var i = 1;
  $('#phone-input').inputmask("+7(999)999-99-99");  //static mask

  $('#work_time-input').datetimepicker({
    format: 'LT'
  });  

  $('#work_time-to_input').datetimepicker({
    format: 'LT'
  });

  $('#addPhone').on('click', function(){
    $('.extra-phones').append('<div class="col-md-2"><input class="form-control" id="phone-input-'+i+'" placeholder="Телефон"  required="required" name="phone[]" type="text"></div>');
    $('#phone-input-'+i).inputmask("+7(999)999-99-99");
    i++;
  })


  $("#allday").change(function() {
    if(this.checked) {
      $('#work_time-to_input').attr('disabled', 'disabled');
      $('#work_time-input').attr('disabled', 'disabled');      
    } else {
      $('#work_time-to_input').attr('disabled', false);
      $('#work_time-input').attr('disabled', false);
    }
});

});


ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [43.2402, 76.9235],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });

    myMap.events.add('click', function (e) {
        if (!myMap.balloon.isOpen()) {
            var coords = e.get('coords');
            var c = [coords[0].toPrecision(6),
                    coords[1].toPrecision(6)].join(', ') 
            myMap.balloon.open(coords, {
                contentHeader:'Событие!',
                contentBody:'<p>Кто-то щелкнул по карте.</p>' +
                    '<p>Координаты щелчка: ' + c + '</p>',
                contentFooter:'<sup>Щелкните еще раз</sup>'
            });
            $('#coordinates_input').val(c);
        }
        else {
            myMap.balloon.close();
        }
    });
}
  </script>
@endsection
