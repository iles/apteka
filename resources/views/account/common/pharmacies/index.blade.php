@extends('account.admin.layout.admin')
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
    <style>
        @media (max-width: 700px) {
            #example2 {
                font-size: 10px;
            }

            #example2 td a {
                font-size: 10px;
            }

            img {
                height: 50px;
            }
        }

        @media (max-width: 507px) {
            #example2 .btn {
                font-size: 7px;
                padding: 1px 1px;
            }
            #example2 td a {
                font-size: 7px;
            }

            #example2 {
                font-size: 7px;
            }
            img {
                height: 30px;
            }
        }
    </style>
</head>
@section('content')
    <section class="content-header">
        <h1>
        Аптеки
        {{--<small> <a href="/pharmacy/create/c"><i class="fas fa-plus"></i> Добавить</a></small>--}}

        <small> <a href="/pharm/create"><i class="fa fa-plus"></i> Добавить</a></small>

      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Главная</a></li>--}}
        {{--<li class="active"><a href="#">Все аптеки</a></li>--}}
      {{--</ol>--}}
    </section>

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            @if($pharmacies->isEmpty())
              <h3>Нет данных</h3>
            @else

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Название</th>
                  <th>Инфо</th>
{{--                   <th>Лого</th> --}}
                  <th>Координаты</th>
                  <th>Владелец</th>
                  <th>Рейтинг</th>
                  <th>Статус</th>
                  @can('pharmacy_control')
                    <th>Действия</th>
                  @endcan
                </tr>
                </thead>
                <tbody>
                  @foreach ($pharmacies as $pharmacy)
                <tr>
                  <td>{{ $pharmacy->title }}</td> 
                  <td>{{ $pharmacy->info }}</td> 
{{--                   <td><img src='/uploads/logos/{{ $pharmacy->logo }}' height="100px" /></td>  --}}
                  <td>{{ $pharmacy->coordinates }}</td> 
                  <td>{{ $pharmacy->user->name }}</td> 
                  <td>{{ $pharmacy->rating }}</td> 
                  <td>{{ $pharmacy->status_id }}</td> 
                  <td>

                    <a href="{{ action('PharmacyController@edit', $pharmacy->id) }}" ><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="{{ action('PriceListController@create', $pharmacy->id) }}" ><span class="glyphicon glyphicon-refresh"></span></a>
                      <form action="/pharmacy/destroy/{{ $pharmacy->id }}" method="POST">
                          @csrf
                          <input value="{{ $pharmacy->id }}" hidden name="id">
                          <a type="submit""><span class="glyphicon glyphicon-trash"></span></a>
                      </form>
                    {{--<a href="{{ action('PharmacyController@destroy', $pharmacy->id) }}" class="btn btn-warning">Удалить</a>--}}
                  </td> 
                  @can('pharmacy_control')
                    <td width="70" style="font-size: 36px">
                      @if( $pharmacy->status_id == 1)
                          <a style="color: #23c374;" href="{{ action('PharmacyController@aprove', ['id'=>$pharmacy->id, 'status' => 3]) }}"><i class="fa fa-thumbs-up"></i></a>
                          <a style="color: #d53737;" href="{{ action('PharmacyController@aprove', ['id'=>$pharmacy->id, 'status' => 2]) }}"><i class="fa fa-times"></i></a>
                      @elseif($pharmacy->status_id == 2)
                          <a style="color: #23c374;" href="{{ action('PharmacyController@aprove', ['id'=>$pharmacy->id, 'status' => 3]) }}"><i class="fa fa-thumbs-up"></i></a>
                    
                      @elseif($pharmacy->status_id == 3)
                         <a style="color: #d53737;" href="{{ action('PharmacyController@aprove', ['id'=>$pharmacy->id, 'status' => 2]) }}"><i class="fa fa-times"></i></a>
                      @endif
                    </td> 
                  @endcan
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection
