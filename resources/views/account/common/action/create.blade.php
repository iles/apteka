@extends('account.admin.layout.admin')

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить акцию</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('action' => 'ActionController@store', 'files'=>true)) }}

              <div class="box-body">

              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="title-input">Название</label>
                  <input class="form-control" id="title-input" placeholder="Название" name="title" type="text">
                  <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
                <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                  <label for="info-input">Текст</label>
                  <textarea name="text" class="form-control"></textarea>
                  <small class="text-danger">{{ $errors->first('text') }}</small>
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                  <label for="logo-input">Изображение</label>
                  <input id="logo-input" name='image' type="file">
                  <p class="help-block">Максимальный размер 2мб</p>
                  <small class="text-danger">{{ $errors->first('image') }}</small>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            </form>
          </div>
@endsection

