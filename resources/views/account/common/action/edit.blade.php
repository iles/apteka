@extends('account.admin.layout.admin')

@section('header-scripts')
 <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
@endsection

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Редактировать акцию</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('action' => 'ActionController@store', 'files'=>true)) }}

              <div class="box-body">

              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
                <div class="form-group">
                  <label for="title-input">Название</label>
                  <input class="form-control" id="title-input" placeholder="Название" name="title" value="{{ $action->title }}" type="text">
                </div>
                <div class="form-group">
                  <label for="info-input">Текст</label>
                  <textarea name="text" class="form-control"> {{ $action->text }}"</textarea>
                </div>
                <div class="form-group">
                  <label for="logo-input">Изображение</label>
                  <input id="logo-input" name='image' type="file" required>
                  <p class="help-block">Максимальный размер 2мб</p>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            </form>
          </div>
@endsection

