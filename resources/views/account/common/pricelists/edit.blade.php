@extends('account.admin.layout.admin')

@section('content')
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить аптеку</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="/price/update" method="post" />
              {!! csrf_field() !!}
              <div class="box-body">
                <div class="form-group">
                  <input class="form-control" id="title-input" placeholder="Название" name="title" type="text" value="{{ $pricelist->pharmacy_id}}">
                  <input name="id" type="text" hidden value="{{ $pricelist->id }}">
                </div>
                <div class="form-group">
                  <label for="info-input">Инфо</label>
                  <textarea class="form-control" id="info-input" placeholder="Инфо" name="info" value="" type="text">
                    {{ $pricelist->drugs }}
                  </textarea>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            </form>
          </div>


@endsection