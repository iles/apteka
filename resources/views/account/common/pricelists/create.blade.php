@extends('account.admin.layout.admin')

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить прайс</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('action' => 'PriceListController@update')) }}
                <div class="box-body">
                  <div class="form-group">
                    <label for="logo-input">Прайс лист</label>
{{--                     <textarea class="form-control" name="drugs"></textarea> --}}
                    <input id="logo-input" name='user_image' type="file">
                    <p class="help-block">Файл в формате *.xml</p>
                  </div>      
{{--                   <input id="logo-input" name='pharmacy_id' hidden value="{{ $pharmacy_id }}"> --}}

                </div>



              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            </form>
</div>


@endsection