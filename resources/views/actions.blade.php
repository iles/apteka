@extends('layouts.app')


@section('content')
<header>	
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="logo-box">
					<button class="menu-btn menu-btn-mobile"><i class="fa fa-bars"></i></button>
					@if(  !Cookie::get('light') )
					<a href="/"><img src="../img/logo.svg" alt="/"></a>
					@endif
				</div>
			</div>
			<div class="col-md-8">
				
				<div class="search-box search-box-inside">
					
					{{ Form::open(array('action' => 'HomeController@find', 'id'=>'form')) }}
					{!! csrf_field() !!}
					<div class="form-group">
						<div class="input-group">
							<select class="meds-select form-group" required multiple="multiple" name="meds[]"></select>
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit" >
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</div>
					<p class="meds-examples">Например: 
						@foreach($meds as $med)
						@if ($loop->last)
						или <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>
						@else
						<a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>,
						@endif
						@endforeach
					</p>
					{{ Form::close() }}
					
				</div>  
				
			</div>
			<div class="col-md-1 hidden-xs">
				<button class="menu-btn"><i class="fa fa-bars"></i></button>
			</div>
			<div class="absolutebutton">
				<button class="menu-btn"><i class="fa fa-bars"></i></button>
			</div>
		</div>
	</div>
</header>
    <section class="crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="/">Главная</a></li> /
                        <li class="active">Акции</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
	<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					<h1 class="title">Акции</h1>
					<div class="action-slider owl-carousel hidden-xs">
						<div class="item"><img src="/img/slider/1.png" alt=""></div>
						<div class="item"><img src="/img/slider/2.png" alt=""></div>
						<div class="item"><img src="/img/slider/3.png" alt=""></div>
					</div>


									
				</div>
				<div class="col-md-2">
					<div class="banner-block">
					</div>

				</div>
			</div>
		</div>
	</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 my-auto" style="margin-top: 10px;">
				<p>eApteka.kz 2017</p>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-8">
				<div class="f-box f-menu">
					<p>
						<ul>
							<li><a href="#">Бренды</a></li>
							<li><a href="/posts">Новости</a></li>
							<li><a href="/actions">Акции</a></li>
							<li><a href="#">Партнеры</a></li>
							<li><a href="#">Контакты</a></li>
						</ul>
					</p>
				</div>      
			</div>
		</div>
	</div>
</footer>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
	  $(".owl-carousel").owlCarousel();
	  showBanner('.banner-block');
	});
</script>
@endsection
