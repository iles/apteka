@extends('layouts.app')
@section('content')
<div class="absolutebutton">
				<button class="menu-btn"><i class="fa fa-bars"></i></button>
		</div>
	<div class="search-main-block">
		<div class="logo-main">
			<a href="">
				{!! Cookie::get('light') == 1 ? '' : '<img src="img/logo.svg" width="270" alt="">' !!}
			
			</a>
		</div>
		<div class="search-box search-box-main">
			<form id="form" method="post" action='/search'>
				{!! csrf_field() !!}

			<div class="form-group" style="width: 100%;">
				<div class="input-group">
				<select class="meds-select form-group" required multiple="multiple" name="meds[]"></select>
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit" >
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</div>

                <p class="examples meds-examples">Например: 
                	@foreach($meds as $med)
	                	@if ($loop->last)
							или <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>
						@else
							<a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>,
						@endif
                	@endforeach
                </p>
            </form>
		</div>
	</div>	
@endsection