<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<title>Главная - Аптека</title>
<!-- <base href="http://okna.funnyschool.kz/" /> -->
<meta name="language" content="ru">
<meta name="keywords" content="Главная" />
<meta name="description" content=""/>
<meta name="robots" content="index, follow" />  
<meta name="HandheldFriendly" content="true"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="apple-mobile-web-app-capable" content="YES" />

<link href='assets/cache/images/32x32-favicon.1cb.png' rel='icon' type='image/x-icon'/>
<link rel="apple-touch-icon" href="assets/cache/images/32x32-favicon.1cb.png">
<link rel="apple-touch-icon" sizes="57x57" href="assets/cache/images/57x57-favicon.3ec.png" />
<link rel="apple-touch-icon" sizes="72x72" href="assets/cache/images/72x72-favicon.4ff.png" />
<link rel="apple-touch-icon" sizes="76x76" href="assets/cache/images/76x76-favicon.874.png" />
<link rel="apple-touch-icon" sizes="114x114" href="assets/cache/images/114x114-favicon.8e0.png" />
<link rel="apple-touch-icon" sizes="120x120" href="assets/cache/images/120x120-favicon.414.png" />
<link rel="apple-touch-icon" sizes="144x144" href="assets/cache/images/144x144-favicon.e84.png" />
<link rel="apple-touch-icon" sizes="152x152" href="assets/cache/images/152x152-favicon.741.png" />

<meta property="og:site_name" content="Аптека">
<meta name="twitter:site" content="Аптека">
<!-- <meta property="og:image" content="http://okna.funnyschool.kz/"/> -->
<meta property="og:title" content="Главная - Аптека">
<meta property="og:description" content="">
<!-- <meta property="og:url" content="http://okna.funnyschool.kz/"> -->



<meta name="DC.publisher" content="Аптека" >
<!-- <meta name="DC.publisher.url" content="http://okna.funnyschool.kz/" >  -->
<meta name="DC.title" content="Главная - Аптека" >
<meta name="DC.description" content="" >
<meta name="DC.coverage" content="World" > 
<meta name="DC.format" content="text/html" > 
<meta name="DC.identifier" content="/" > 



    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/libs/animate/animate.css">
    <link rel="stylesheet" href="/css/libs/magnific/magnific-popup.css">
    <link rel="stylesheet" href="/css/libs/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">


    <link rel="stylesheet" href="/css/pushy.css">
    <link rel="stylesheet" href="/css/css/fonts.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/select2-bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/css/media.css">

    <script src="/js/modernizr.js"></script>

<script src="/js/jquery-3.3.1.min.js"></script>

@yield('header-scripts')
</head>
<body style="min-height: calc(100vh - 32px);"> 
@yield('modal')
        <nav class="pushy pushy-left">
            <div class="pushy-content">
                <ul>
                    <li class="pushy-link"><a href="brands.html">Бренды</a></li>
                    <li class="pushy-link"><a href="/posts">Новости</a></li>
                    <li class="pushy-link"><a href="/actions">Акции</a></li>
                    <li class="pushy-link"><a href="#">Партнеры</a></li>
                    <li class="pushy-link"><a href="#">Контакты</a></li>
                 </ul>
            </div>
        </nav>
    @include('layouts.header')
    
<a href="#" id="back-to-top" title="Back to top">&uarr;</a>
    @yield('content')
<script src="/js/bootstrap.min.js"></script>
<script src="/libs/waypoints/waypoints.min.js"></script>
<script src="/libs/animate/animate-css.js"></script>
<script src="/libs/plugins-scroll/plugins-scroll.js"></script>
<script src="/libs/magnific/jquery.magnific-popup.min.js"></script>
<script src="/libs/owl.carousel/owl.carousel.min.js"></script>
<!-- <script src="/libs/jVForms/jVForms.min.js"></script> -->
<script src="/libs/pushy/js/pushy.min.js"></script>

<script src="/js/select2.min.js"></script>
<script src="/js/doT.min.js"></script>
<script src="/js/ua-parser.min.js"></script>
<script src="/js/common.js"></script>

@yield('scripts')

</body>
</html>
