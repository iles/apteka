@extends('layouts.app')


@section('modal')
<div class="modal fade" id="commentModal" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog">
    <div class="modal-content">
    	{{ Form::model($comment, array('route' => 'comment.store' )) }}   

      <div class="modal-header">
        <h5 class="modal-title" >Отзыв</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	{{ Form::label($comment->name, 'Имя') }}
      	 {{ Form::text('name', '', ['class' => 'form-control']) }}
      	 {{ Form::label($comment->email, 'Email') }}
      	 {{ Form::text('email', '', ['class' => 'form-control']) }}
      	 {{ Form::label($comment->text, 'Текст') }}
      	 {{ Form::textarea('text', '', ['class' => 'form-control']) }}
      	 <input type="text" hidden name="pharmacy_id" value="{{ $pharmacy->id}}" >
      	 {{--<input type="text" hidden name="rating" value="4" >--}}
          <div class="star-rating"> 
              <div class="star-rating__wrap">
                  <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5">
                  <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="5 out of 5 stars"></label>
                  <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4">
                  <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="4 out of 5 stars"></label>
                  <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3">
                  <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="3 out of 5 stars"></label>
                  <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2">
                  <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="2 out of 5 stars"></label>
                  <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1">
                  <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="1 out of 5 stars"></label>
              </div>
          </div>

      </div>
      <div class="modal-footer">
        {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
        <button type="submit" class="btn btn-primary">Отправить</button>
      </div>
  {{ Form::close() }}
    </div>
  </div>
</div>
@endsection


@section('content')
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo-box">
                        <button class="menu-btn menu-btn-mobile"><i class="fa fa-bars"></i></button>
                        @if(  !Cookie::get('light') )
                             <a href="/"><img src="../img/logo.svg" alt="/"></a>
                        @endif
                    </div>
                </div>
                <div class="col-md-8">
                    
                        <div class="search-box search-box-inside">
                        
                        {{ Form::open(array('action' => 'HomeController@find', 'id'=>'form')) }}
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <div class="input-group">
                                <select class="meds-select form-group" required multiple="multiple" name="meds[]"></select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" >
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <p class="meds-examples">Например: 
                                @foreach($meds as $med)
                                    @if ($loop->last)
                                        или <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>
                                    @else
                                        <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>,
                                    @endif
                                @endforeach
                            </p>
                        {{ Form::close() }}
                                       
                        </div>  
                    
                </div>
                <div class="col-md-1 hidden-xs">
                        <button class="menu-btn"><i class="fa fa-bars"></i></button>
                </div>
                <div class="absolutebutton">
                    <button class="menu-btn"><i class="fa fa-bars"></i></button>
                </div>
            </div>
        </div>
    </header>
    <section class="crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="/">Главная</a></li> /
                        <li class="active">{{ $pharmacy->title }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<section class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					<h1 class="title title-pharmacy">{{ $pharmacy->title }}</h1>
					<div class="vote-box vote-pharmacy">
                        <ul class="star" >
                            @if ($rating == 1)
                                <h3>Нет оценок</h3>
                            @elseif ($rating == 2)
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            @elseif ($rating == 3)
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            @elseif ($rating == 4)
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            @elseif ($rating == 5)
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            @elseif ($rating == 0)
                                <h1 class="title title-pharmacy" >Нет оценок</h1>
                            @endif
                        </ul>
					</div>

					<div class="pharmacy pharmacy-mobile">
						 <div class="tabs hidden-lg hidden-md hidden-sm">
					        <a href="[~[*parent*]~]" class="tab">О компании</a>
					    </div>
				    </div>

                    {{--*********--}}
					<div class="search-pharmacy">
						<h3>Поиск по аптеке</h3>
						<div class="search-box search-box-inside">
                            {{ Form::open(array('action' => 'HomeController@find_pharmacy')) }}
                            {!! csrf_field() !!}
                            <input type="text" name="pharmacy_id" hidden value="{{ $pharmacy->id }}">
                             <div class="form-group">
                                    <div class="input-group">
                                    <select class="meds-select form-group" required multiple="multiple" name="meds[]"></select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit" >
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            <p class="meds-examples">Например: 
                                @foreach($meds as $med)
                                    @if ($loop->last)
                                        или <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>
                                    @else
                                        <a href="#"  data-id="{{$med->id}}">{{$med->drug_title}}</a>,
                                    @endif
                                @endforeach
                            </p>
                            {{ Form::close() }}
                        </div>
					</div>

                    {{--<div class="search-main-block">--}}
                        {{--<div class="logo-main"><a href=""><img src="img/logo.jpg" width="270" alt=""></a></div>--}}
                        {{--<div class="search-box search-box-main">--}}
                            {{--<form id="form" method="post">--}}
                                {{--{!! csrf_field() !!}--}}
                                {{--<input type="text" name="pharmcay_id" hidden value="{{ $pharmacy->id }}">--}}
                                {{--<input type="text" name="search" id="keyword" placeholder="Введите название лекарства для поиска по аптекам Алматы" >--}}
                                {{--<button type="submit" class="btn btn-green vf-submit"><i class="fa fa-search" aria-hidden="true" style=" line-height: 1.52857143; "></i>Поиск</button>--}}
                                {{--<p class="examples">Например: <a href="#">звёздочка</a>, <a href="#" >натуральные масла</a>, <a href="#">аспирин</a> или <a href="#">панадол</a></p>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}



                    <div class="pharmacy">
						<ul class="nav nav-tabs" role="tablist">
                            @if($flag)
                                <li role="presentation"><a href="#drugs" aria-controls="drugs" role="tab" data-toggle="tab">Препараты</a></li>
                            @endif
						    <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">О компании</a></li>
						    <li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Комментарии</a></li>
                        </ul>

                        <div class="tab-content">
                            @if($flag)
                            <div role="tabpanel" class="tab-pane fade" id="drugs">
                                @foreach($data as $dt)
                                    <div class="tab_item">
                                        <div class="result-item">
                                            <div class="top">
                                                <h3>{{$dt->drug_title}}</h3>
                                                <h4>{{$dt->price}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @endif

						   <div role="tabpanel" class="tab-pane fade in active" id="about">
					            <div class="tab_item">
							 	    <div class="result-item">
									    <div class="top">
										<div class="name"><a href="apteka.html">{{ $pharmacy->title }}</a></div>
										<div class="add"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $pharmacy->address }}</div>
										<div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $pharmacy->time_work }}</div>
										<div class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+77172573176">{{ $pharmacy->phone }}</a></div>
									</div>
									<div class="map-pharmacy">
										<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A67f34e5360d3b7ec0c49918fc8c47bc60c6aa1c359bec7031f29dc9b932f7438&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
									</div>
								</div>
					         </div>
						  </div>


						  <div role="tabpanel" class="tab-pane fade" id="comments">
						  	<div class="reviews-block" id="reviews">
						  		<a href="#"  data-toggle="modal" data-target="#commentModal">Оставить отзыв</a>
						  		@foreach( $comments as $comment)
							        <div class="reviews-item">
										<div class="author">{{$comment->name}}</div>
										<div class="vote-box vote-pharmacy">
                                            <ul class="star" >
                                                @if ($comment->rating == 1)
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                @elseif ($comment->rating == 2)
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                @elseif ($comment->rating == 3)
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                @elseif ($comment->rating == 4)
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                @elseif ($comment->rating == 5)
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                @endif
                                            </ul>
										</div>
										<div class="reviews-desc">
                                            <p>{{ $comment->text }}</p>
                                        </div>
							        </div>
							    @endforeach
                            </div>
                          </div>
                         </div>
                    </div>
                </div>
				<div class="col-md-2">
                    @if(  !Cookie::get('light') )
                    <div class="banner-block">

                    </div>
                    @endif

				</div>
			</div>
		</div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 my-auto" style="margin-top: 10px;">
                <p>eApteka.kz 2017</p>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="f-box f-menu">
                    <p>
                        <ul>
                            <li><a href="#">Бренды</a></li>
                            <li><a href="/posts">Новости</a></li>
                            <li><a href="/actions">Акции</a></li>
                            <li><a href="#">Партнеры</a></li>
                            <li><a href="#">Контакты</a></li>
                        </ul>
                    </p>
                </div>      
            </div>
        </div>
    </div>
</footer>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
	  $(".owl-carousel").owlCarousel();
              showBanner('.banner-block');
	});

        // z1.onclick = function() {
        //     document.getElementById("stars").innerHTML =
        //         "              <li><i class=\"fa fa-star\" id=\"z1\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z2\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z3\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z4\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z5\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <input type=\"text\" hidden name=\"rating\" value=\"4\" >"
        // };
        // z2.onclick = function() {
        //     document.getElementById("stars").innerHTML =
        //         "              <li><i class=\"fa fa-star\" id=\"z1\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z2\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z3\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z4\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z5\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <input type=\"text\" hidden name=\"rating\" value=\"4\" >"
        // };
        // z3.onclick = function() {
        //     document.getElementById("stars").innerHTML =
        //         "              <li><i class=\"fa fa-star\" id=\"z1\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z2\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z3\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z4\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z5\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <input type=\"text\" hidden name=\"rating\" value=\"4\" >"
        // };
        // z4.onclick = function() {
        //     document.getElementById("stars").innerHTML =
        //         "              <li><i class=\"fa fa-star\" id=\"z1\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z2\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z3\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z4\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star-o\" id=\"z5\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <input type=\"text\" hidden name=\"rating\" value=\"4\" >"
        // };
        // z5.onclick = function() {
        //     document.getElementById("stars").innerHTML =
        //         "              <li><i class=\"fa fa-star\" id=\"z1\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z2\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z3\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z4\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <li><i class=\"fa fa-star\" id=\"z5\" aria-hidden=\"true\"></i></li>\n" +
        //         "              <input type=\"text\" hidden name=\"rating\" value=\"4\" >"
        // };


</script>
@endsection

