@extends('layouts.app')


@section('content')
<section class="content">
	<section class="cabinet-wrap">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3 col-lg-2 p0">

				</div>
				<div class="col-md-9 col-lg-10	 p0">	
					<div class="cab-content">
						<h1>Статистика по действующим кампаниям</h1>
						<p><input type="text" placeholder="Поиск по кампаниям"> <button class="btn btn-green btn-small"><i class="fa fa-search" aria-hidden="true"></i> Искать</button></p>
						<p><a href="cab-new-company.html" class="btn btn-green"><i class="fa fa-briefcase" aria-hidden="true"></i> Создать новую кампанию</a> <a href="cab-advert-user.html" class="btn btn-green"><i class="fa fa-reply" aria-hidden="true"></i> Назад к списку кампаний</a></p>
						<table class="reviews-list advert-table">
							<tr>
								<th>Превью кампании</th>
								<th>Наименование кампании</th>
								<th>Тип кампании</th>
								<th>Дата начала</th>
								<th>Дата окончания</th>
								<th>Бюджет</th>
								<th>Остаток средств</th>
								<th>Статус оплаты</th>
								<th>Количество показов</th>
								<th>Количество кликов</th>
								<th>Действие</th>
							</tr>
							<tr>
								<td><img src="/assets/templates/site/img/ban.png" width="70"  alt=""></td>
								<td>Рекламный баннер</td>
								<td>Баннерная реклама</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 15.10.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 14:55</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 30.11.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 18:24</td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>84 000 тг</strong></td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>24 333 тг</strong></td>
								<td><p class="infoline green">Оплачено</p></td>
								<td><span class="green"><i class="fa fa-eye" aria-hidden="true"></i> 150</span></td>
								<td><span class="yellow"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> 48</span></td>
								<td><button class="btn btn-green btn-small">Включить</button> <button class="btn btn-yellow btn-small">Выключить</button> <a class="btn btn-yellow btn-small" href="cab-new-company.html">Изменить</a></td>
							</tr>
							<tr>
								<td><img src="/assets/templates/site/img/ban.png" width="70"  alt=""></td>
								<td>Рекламный баннер</td>
								<td>Баннерная реклама</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 15.10.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 14:55</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 30.11.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 18:24</td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>84 000 тг</strong></td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>24 333 тг</strong></td>
								<td><p class="infoline green">Оплачено</p></td>
								<td><span class="green"><i class="fa fa-eye" aria-hidden="true"></i> 150</span></td>
								<td><span class="yellow"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> 48</span></td>
								<td><button class="btn btn-green btn-small">Включить</button> <button class="btn btn-yellow btn-small">Выключить</button> <a class="btn btn-yellow btn-small" href="cab-new-company.html">Изменить</a></td>
							</tr>
							<tr>
								<td><img src="/assets/templates/site/img/ban.png" width="70"  alt=""></td>
								<td>Рекламный баннер</td>
								<td>Баннерная реклама</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 15.10.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 14:55</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 30.11.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 18:24</td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>84 000 тг</strong></td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>24 333 тг</strong></td>
								<td><p class="infoline green">Оплачено</p></td>
								<td><span class="green"><i class="fa fa-eye" aria-hidden="true"></i> 150</span></td>
								<td><span class="yellow"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> 48</span></td>
								<td><button class="btn btn-green btn-small">Включить</button> <button class="btn btn-yellow btn-small">Выключить</button> <a class="btn btn-yellow btn-small" href="cab-new-company.html">Изменить</a></td>
							</tr>
							<tr>
								<td><img src="/assets/templates/site/img/ban.png" width="70"  alt=""></td>
								<td>Рекламный баннер</td>
								<td>Баннерная реклама</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 15.10.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 14:55</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 30.11.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 18:24</td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>84 000 тг</strong></td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>24 333 тг</strong></td>
								<td><p class="infoline green">Оплачено</p></td>
								<td><span class="green"><i class="fa fa-eye" aria-hidden="true"></i> 150</span></td>
								<td><span class="yellow"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> 48</span></td>
								<td><button class="btn btn-green btn-small">Включить</button> <button class="btn btn-yellow btn-small">Выключить</button> <a class="btn btn-yellow btn-small" href="cab-new-company.html">Изменить</a></td>
							</tr>
							<tr>
								<td><img src="/assets/templates/site/img/ban.png" width="70"  alt=""></td>
								<td>Рекламный баннер</td>
								<td>Баннерная реклама</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 15.10.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 14:55</td>
								<td><i class="fa fa-calendar" aria-hidden="true"></i> 30.11.2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 18:24</td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>84 000 тг</strong></td>
								<td><i class="fa fa-money" aria-hidden="true"></i> <strong>24 333 тг</strong></td>
								<td><p class="infoline green">Оплачено</p></td>
								<td><span class="green"><i class="fa fa-eye" aria-hidden="true"></i> 150</span></td>
								<td><span class="yellow"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> 48</span></td>
								<td><button class="btn btn-green btn-small">Включить</button> <button class="btn btn-yellow btn-small">Выключить</button> <a class="btn btn-yellow btn-small" href="cab-new-company.html">Изменить</a></td>
							</tr>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
	  $(".owl-carousel").owlCarousel();
	});
</script>
@endsection
