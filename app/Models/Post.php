<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
       'title', 'body', 'created_at',
    ];

    public function pharmacy()
    {
        return $this->belongsTo('App\Models\Pharmacy');
    }
    public function savePost($data, $imageName)
    {
        $this->logo = $imageName;
        $this->title = $data['title'];
        $this->body = $data['body'];
        $this->save();
        return 1;
    }
}
