<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Drug extends Model
{
   	protected $fillable = [
        'pharmacy_id', 'price_json',
    ];


    public function priceLists()
    {
        return $this->hasMany('App\Models\PriceList');
    }


}
