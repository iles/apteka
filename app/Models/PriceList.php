<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PriceList extends Model
{
   	protected $fillable = [
        'pharmacy_id', 'price_json',
    ];

	public function pharmacies ()
    {
    	return $this->hasMany('App\Models\Pharmacy');
    }

    public function drugs()
    {
        return $this->hasMany('App\Models\Drug');
    }
}
