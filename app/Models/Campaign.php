<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'pharmacy_id', 'title', 'type', 'date_start', 'date_end', 'status_id',
    ];
    public function getBanner()
    {
        return $this->hasMany('App\Models\PriceList');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
   	protected $guarded = [
        'status_id', 'rest', 'payment_status_id', 'show_amount', 'click_amount',
    ];

}
