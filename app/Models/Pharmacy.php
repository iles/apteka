<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
	protected $fillable = [
        'title', 'info', 'user_image', 'is_online', 'coordinates', 'rating', 'phone', 'time_work', 'address',
    ];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // public function priceLists()
    // {
    //     return $this->hasMany('App\Models\PriceList');
    // }    

    // public function drugs()
    // {
    //     return $this->belongsToMany('App\Models\Drug', 'price_lists')->withPivot('price')->withTimestamps();
    // }

    public function drugs()
    {
        return $this->belongsToMany('App\Models\Drug', 'price_lists')->withPivot('price')->orderBy('pivot_price', 'DESC');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }


    public function savePharmacy($data, $imageName)
    {
        $this->user_id = auth()->user()->id;
        $this->status_id = 1;
        $this->title = $data['title'];
        $this->info = $data['info'];
        $this->logo = $imageName;
        if(isset($data['is_online']) ){
            $this->is_online = $data['is_online'];            
        }        
        if(isset($data['allday']) ){
            $this->time_work = '00:00 - 00:00';
        } else {
            $this->time_work = $data['time_work'].' - '.$data['time_work2'];
        }
        $this->phone = implode(';', $data['phone']);
        $this->address = $data['address'];
        $this->coordinates = $data['coordinates'];
        $this->save();
        return 1;
    }
    public function savePharmacyData($data)
    {
        $this->user_id = auth()->user()->id;
        $this->status_id = 1;
        $this->title = $data['title'];
        $this->info = $data['info'];
        if(isset($data['is_online']) ){
            $this->is_online = $data['is_online'];
        }
        $this->phone = $data['phone'];
        $this->time_work = $data['time_work'];
        $this->address = $data['address'];
        $this->coordinates = $data['coordinates'];
        $this->save();
        return 1;
    }
}
