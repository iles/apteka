<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
   	protected $guarded = [
        '',
    ];

     public function pharmacy()
	 {
	   return $this->belongsTo('App\Models\Pharmacy');
	 }
}
