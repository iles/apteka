<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
	protected $fillable = [
        'title', 'image', 'text'
    ];

    public function saveAction($data, $imageName)
    {

        $this->title = $data['title'];
        $this->text = $data['text'];
        $this->image = $imageName;

        $this->save();
        return 1;
    }
}
