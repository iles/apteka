<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
   	protected $fillable = [
        'campaign_id', 'image', 'title', 'text', 'pharmacy_id',
        ];

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign');
    }

    public function pharmacy()
    {
        return $this->hasOne('App\Models\Pharmacy');
    }

}
