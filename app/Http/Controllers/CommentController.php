<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->comment = new Comment;
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'text' => 'required',
            'rating' => 'integer',
            'pharmacy_id' => 'integer',
        ]);
        Comment::create($data);

//        $this->comment->fill($request->all());
//        $this->comment->save();
        return back();
    }


}
