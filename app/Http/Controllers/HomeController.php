<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pharmacy;
use App\Models\Comment;
use App\Models\Drug;
use App\Models\PriceList;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use App\Models\Action;
use PhpParser\ErrorHandler\Collecting;
use Redirect;
use Cookie;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function meds(Request $request)
    {
        $data = Drug::take(3)->where('drug_title', 'like', '%'.$request->q.'%')->select('id', 'drug_title AS text')->get();
        return $data->toJson(JSON_PRETTY_PRINT);
    }      

    public function medstop(Request $request)
    {
        $data = Drug::take(2)->select('id', 'drug_title AS text')->get();
        return $data->toJson(JSON_PRETTY_PRINT);
    }      

    public function news()
    {
        $news = Post::paginate(5);
        $meds = Drug::take(4)->orderBy('search', 'DESC')->get(); 

        return view('news', compact('news', 'meds'));
    }

    public function newsShow($id)
    {
        $post = Post::findOrFail($id);
        $meds = Drug::take(4)->orderBy('search', 'DESC')->get();

        return view('posts.show', compact('post', 'meds'));
    }

    public function index()
    {
        $meds = Drug::take(4)->orderBy('search', 'DESC')->get();
        return view('index', compact('meds'));
    }    

    public function pharmacy($slug, Request $request)
    {
        $flag = false;
        $pharmacy = Pharmacy::where('slug', $slug)->first();
        $comment = new Comment;
        $comments = Pharmacy::find($pharmacy->id)->comments;
        $comments =  $comments->sortByDesc('id');

        $count = 0;
        $sum = 0;
        foreach($comments as $comm){
            $count = $count + 1;
            $sum = $sum + $comm->rating;
        }

        if ($count == 0) {
            $rating = 0;
        } else {
            $rating = round($sum / $count);
        }
        $meds = Drug::take(4)->orderBy('search', 'DESC')->get(); 

        return view('pharmacy', compact('pharmacy', 'comment', 'comments', 'rating', 'meds', 'flag' ) );
    }




    public function light()
    {   
        $c = Cookie::get('light');
        if($c){
            Cookie::queue(Cookie::forget('light'));
        } else {
            Cookie::queue(Cookie::make('light', '1', 6000));
        }
         return redirect()->back();
    }  


    public function actions()
    {
        $actions = Action::paginate(5);
        $meds = Drug::take(4)->orderBy('search', 'DESC')->get();
        return view('actions', compact('actions', 'meds') );
    }

    public function find_pharmacy(Request $request)
    {

        $flag = true;
        $keyword = $request->search;
        $keyword = mb_convert_case($keyword, MB_CASE_LOWER, "UTF-8");
        $comment = new Comment;


        $pharmacy = Pharmacy::where('id', $request->pharmacy_id)->first();

        $comments = Pharmacy::find($request->pharmacy_id)->comments;

        $count = 0;
        $sum = 0;
        foreach($comments as $comm){
            $count = $count + 1;
            $sum = $sum + $comm->rating;
        }

        if ($count == 0) {
            $rating = 0;
        } else {
            $rating = round($sum / $count);
        }

        $words =  explode(' ', $keyword);

        foreach($words as $word) {
            $data = Pharmacy::join('price_lists', 'pharmacies.id', '=', 'price_lists.pharmacy_id')
                ->join('drugs', 'drugs.id', '=', 'price_lists.drug_id')
//            ->whereRaw('price_lists.pharmacy_id = ? and drugs.drug_title = ?', [$request->pharmacy_id, $keyword])
                ->where('price_lists.pharmacy_id', $request->pharmacy_id)
                ->where('drugs.drug_title', $word)
                ->select('pharmacies.id', 'title', 'logo', 'drugs.id as dr_id', 'drugs.drug_title', 'price_lists.price')
                ->get();
        }

        $meds = Drug::take(4)->orderBy('search', 'DESC')->get(); 

        return view('pharmacy', compact('drugs', 'data', 'flag', 'comment','comments', 'meds', 'rating', 'pharmacy'));
    }

    public function find(Request $request)
    {
        $sort = $request->sort ? $request->sort : 'asc';
        $ids = $request->meds;
        
        if(!$ids){
            $meds = Drug::take(4)->orderBy('search', 'DESC')->get();
            return redirect('index');            
        }


        $drugs = Drug::whereIn('id', $ids)->get();

       
        $query = Pharmacy::with('drugs');

        foreach($ids as $id){
            $query->whereHas('drugs', function($q) use ($id){
                $q->where('drug_id', $id);
            });
        }

        $data = $query->get();

        $pharm_id = [];

        foreach ($data as $d ) {
            $pharm_id[] = $d->id;
        }

        $prices = PriceList::whereIn('drug_id', $ids)->whereIn('pharmacy_id', $pharm_id)->distinct()->get();

        DB::table('drugs')
           ->whereIn('id', $ids)
           ->update([
               'search' => DB::raw('search + 1'),
           ]);

        $meds = Drug::take(4)->orderBy('search', 'DESC')->get();


        foreach ($data as $pharmacy){

            foreach($prices as $price){                   
                if($price->pharmacy_id == $pharmacy->id){
                    foreach($drugs as $drug){
                        if($price->drug_id == $drug->id){
                            $pharmacy->drug_title = $drug->drug_title;
                            $pharmacy->drug_price = $price->price;
                        }                        
                    }
                }
            }
        }


        if($sort == 'asc'){
            $data = $data->sortBy('drug_price');
        } else {
            $data = $data->sortByDesc('drug_price');
        }

        return view('result', compact('data', 'sort', 'drugs', 'prices', 'ids', 'meds'));
    }

    public function search_polygon(Request $request){
        $ids = $request->meds;
        $drugs = Drug::whereIn('id', $ids)->get();
       
        $query = Pharmacy::with('drugs');

        foreach($ids as $id){
            $query->whereHas('drugs', function($q) use ($id){
                $q->where('drug_id', $id);
            });
        }

        $data = $query->select('pharmacies.id', 'pharmacies.coordinates' )->get();
        
        return $data;   
    }    

    public function find_ids(Request $request){
        
        $pids = $request->pids;
        $ids = $request->ids;
        $res = [];

        $drugs = Drug::whereIn('id', $ids)->get();
       
        $query = Pharmacy::with('drugs');

        foreach($ids as $id){
            $query->whereHas('drugs', function($q) use ($id){
                $q->where('drug_id', $id);
            });
        }

        $query->whereIn('pharmacies.id', $pids);

        $data = $query->get();



        $pharm_id = [];

        foreach ($data as $d ) {
            $pharm_id[] = $d->id;
        }

        $prices = PriceList::whereIn('drug_id', $ids)->whereIn('pharmacy_id', $pharm_id)->distinct()->get();

        DB::table('drugs')
           ->whereIn('id', $ids)
           ->update([
               'search' => DB::raw('search + 1'),
           ]);


        $res['data'] = $data;
        $res['prices'] = $prices;
        $res['drugs'] = $drugs;

       return $res;
    }
}
