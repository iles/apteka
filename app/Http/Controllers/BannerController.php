<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Pharmacy;
use App\User;
use Illuminate\Http\Request;
use App\Models\Banner;
use Auth;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->banner = new Banner;
    }

    public function index($id)
    {
        $banners = Banner::where('campaign_id', $id)->get();
        return view('account.common.banners.index', ['banners'=> $banners]);
    }    

    public function showBanner()
    {
        $banners = Banner::inRandomOrder()->take(2)->get();


    }

    public function showad(Request $request)
    {
        $show_cost = 100;
        $banners = Banner::inRandomOrder()->take(2)->get();

        $res = '';

        foreach ($banners as $key => $banner)
        {
            $campaign_id = $banner->campaign_id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            $campaign->show_amount += 1;
            $campaign->budget -= $show_cost;
            $campaign->update();

            $res .= '<div class="ban-box">
                            <a href="click/pharmacy/'.$banner->id.'">
                            <img src="/uploads/banners/'.$banner->image.'" alt="" >
                            <h4>'.$banner->title.'</h4>
                            <p style="color:#2c3e50;">'.$banner->text.'</p>
                            </a>
                        </div>';
        }
        return $res;
    }

    public function clickBanner($id)
    {
        $click_cost = 200;
        $banner = Banner::where('id', $id)->first();
        $campaign_id = $banner->campaign_id;
        $campaign = Campaign::where('id', $campaign_id)->first();
        $campaign->click_amount += 1;
        $campaign->budget -= $click_cost;
        $campaign->update();

        return redirect('pharmacy/'.$banner->pharmacy_id);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create($id)
    {
        $campaign = Campaign::where('id', $id)->get();

        return view('account.common.banners.create', ['banner'=>$this->banner, 'id' => $id, 'campaign' => $campaign]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $this->banner->fill($request->all());

        $id = $request->campaign_id;
        $campaign = Campaign::where('id', $id)->first();
//        dd($campaign);
        $pharmacy_id = $campaign->pharmacy_id;

        if ($request->hasFile('image')) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/banners/'), $imageName); 
             $this->banner->image = $imageName;      
        }
        $this->banner->pharmacy_id = $pharmacy_id;

        $this->banner->save();
        return redirect()->action(
            'BannerController@index', ['id' => $id]
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Action = Action::findOrFail($id);

        return view('account.common.pharmacies.show', compact('Action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $Action = Action::where('id',  '=', $id)->first();

       return view('account.common.action.edit', compact('Action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Action->update($request->all());
        $Action= \App\Action::find($id);
        return redirect('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
