<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Auth;
use Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('account.common.users.index', ['users'=> $users]);
    }    

    public function profile()
    {
        $id = Auth::user()->id;
        $profile = Profile::where('user_id', $id)->first();
        return view('account.common.users.profile', compact('profile'));
    }    

    public function settings()
    {
        $id = Auth::user()->id;
        $user = User::where('id', $id)->first();
        return view('account.common.users.settings', compact('user'));
    }

    public function profileUpdate(Request $request)
    {
        $profile = Profile::where('user_id', '=', Auth::user()->id );
        $this->validate(request(), [
            'firstname'=> 'required',
            'lastname'=>'required',
//            'info'=>'required',
            'patronic'=>'required',
            'phone'=>'integer',
        ],[
            'firstname.required' => 'Заполните поле "Имя"',
            'lastname.required' => 'Заполните поле "Фамилия"',
            'patronic.required' => 'Заполните поле "Информация о вас"',
        ]);
        if ($request->hasFile('user_image')) {
            $imageName = time().'.'.$request->user_image->getClientOriginalExtension();
            $request->user_image->move(public_path('uploads/logos/'), $imageName);
        }
        $profile->update(request(['firstname','lastname','patronic', 'phone', 'imageName']));

        return redirect('/profile')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }    

    public function userUpdate(Request $request)
    {
        $t = '';
        $user = Auth::user();

        if( $request->get('current-password') ){
            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error","Текущий пароль не совпадает. Попробуйте снова.");
            }
            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("error","Новый пароль не может быть такой же как текущий. Выберите другой пароль");
            }
            $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6|confirmed',
            ]);
            
            $user->password = bcrypt($request->get('new-password'));
            
            $t .= 'Пароль успешно обновлен. ';
        }
        if($request->name){
            if($user->name !== $request->name){
                $user->name = $request->name;
                $t .= 'Логин успешно обновлен. ';                
            }
        }        

        if($request->email){
            if($user->email !== $request->email){
                $user->email = $request->email;
                $t .= 'Email успешно обновлен. ';                
            }
        }
        //Change Password
        $user->save();

        return redirect()->back()->with("success", $t);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
