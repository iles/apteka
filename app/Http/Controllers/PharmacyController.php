<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pharmacy;
use Auth;

class PharmacyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = null)
    {
        if(!$status){
            $pharmacies = Pharmacy::where('user_id', Auth::id() )->get();
            //$pharmacies = Pharmacy::all()->sortByDesc('id');            
        } elseif ($status == 'my') {
            $pharmacies = Pharmacy::where('user_id', Auth::id() )->get();
        } elseif ($status == 'new') {
            $pharmacies = Pharmacy::where('status_id', 1)->get();  
        } elseif ($status == 'rejected') {
            $pharmacies = Pharmacy::where('status_id', 2)->get();  
        } elseif ($status == 'active') {
            $pharmacies = Pharmacy::where('status_id', 3)->get();  
        }
        return view('account.common.pharmacies.index', ['pharmacies'=> $pharmacies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account.common.pharmacies.create');
    }

    public function aprove($id, $status)
    {
        $pharmacy = Pharmacy::where('id',  '=', $id)->first();
        $pharmacy->status_id = $status;
        $pharmacy->save();
        return redirect('admin/pharmacies');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pharmacy = new Pharmacy();

        $data = $this->validate($request, [
            'title'=> 'required',
            'info'=>'required',
            'coordinates'=>'required',
            'is_online'=>'integer',
            'phone'=>'required',
            'time_work'=>'string',
            'time_work2'=>'string',
            'address'=>'required',
        ]);

        $imageName = '';
        if ($request->hasFile('user_image')) {
            $imageName = time().'.'.$request->user_image->getClientOriginalExtension();
            $request->user_image->move(public_path('uploads/logos/'), $imageName);
        }


        $pharmacy->savePharmacy($data, $imageName);
        return redirect('admin/pharmacies/my')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pharmacy = Pharmacy::findOrFail($id);
        return view('account.common.pharmacies.show', compact('pharmacy'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $pharmacy = Pharmacy::where('id',  '=', $id)->first();

       return view('account.common.pharmacies.edit', compact('pharmacy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $pharmacy = Pharmacy::where('id',  '=', $id)->first();

        $data = $this->validate($request, [
            'title'=> 'required',
            'info'=>'required',
            'coordinates'=>'required',
            'is_online'=>'integer',
            'phone'=>'required',
            'time_work'=>'required',
            'address'=>'required',
        ]);

        $imageName = '';
        if ($request->hasFile('user_image')) {
            $imageName = time().'.'.$request->user_image->getClientOriginalExtension();
            $request->user_image->move(public_path('uploads/logos/'), $imageName);
            $pharmacy->savePharmacy($data, $imageName);
        } else {
//            $pharmacy->logo->delete();
//            dd($pharmacy);
            $pharmacy->savePharmacyData($data);
        }

        return redirect('admin/pharmacies')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $pharmacy = Pharmacy::findOrFail($id);
        $pharmacy->delete();
        return redirect('admin/pharmacies');
    }
}
