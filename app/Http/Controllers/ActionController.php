<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Action;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actions = Action::all();
        return view('account.common.action.index', ['actions'=> $actions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account.common.action.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Action = new Action();

        $data = $this->validate($request, [
            'title'=> 'required',
            'text'=>'required',
        ]);

        if ($request->hasFile('image')) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/actions/'), $imageName);            
        }


        $Action->saveAction($data, $imageName);
        return redirect('/admin/actions')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Action = Action::findOrFail($id);

        return view('account.common.pharmacies.show', compact('Action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $action = Action::where('id',  '=', $id)->first();

       return view('account.common.action.edit', compact('action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->id;
        $action = Action::where('id',  '=', $id)->first();

        $data = $this->validate($request, [
            'title'=> 'required',
            'text'=>'required',
            'image'=>'required|mimes:jpeg,bmp,png|size:2048',
        ]);

        $imageName = '';
        if ($request->hasFile('user_image')) {
            $imageName = time().'.'.$request->user_image->getClientOriginalExtension();
            $request->user_image->move(public_path('uploads/logos/'), $imageName);
        }

        $action->saveAction($data, $imageName);
        return redirect('admin/pharmacies')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $action = Action::findOrFail($id);
        $action->delete();
        return redirect('admin/actions');
    }
}
