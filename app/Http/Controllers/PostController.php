<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use DB;

class PostController extends Controller
{
    public function index()
    {
        $news = Post::paginate(5);

        return view('account.common.posts.index', compact('news'));
    }

    public function create()
    {
        return view('account.common.posts.create');
    }

    public function store(Request $request)
    {
        $post = new Post();

        $data = $request->validate([
            'title'=> 'required',
            'body'=>'required',

        ]);

        $imageName = '';
        if ($request->hasFile('logo')) {
            $imageName = time().'.'.$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('uploads/news/'), $imageName);
        }

        $post->savePost($data, $imageName);
        return redirect('/admin/posts')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show', compact('post'));
    }

    public function edit(Post $post)
    {
        return view("posts.edit", compact('post'));
    }

    public function update(Post $post, Request $request)
    {
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required',
        ]);
        $post->update(request(['title', 'body']));

        if ($request->hasFile('user_image')) {
            $imageName = time().'.'.$request->user_image->getClientOriginalExtension();
            $request->user_image->move(public_path('uploads/news/'), $imageName);
            $post->logo = $imageName;
            $post->save();
        }
        return redirect('/admin/posts');
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/admin/posts');
    }
}
