<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pharmacy;
use App\Models\PriceList;
use Illuminate\Support\Facades\DB;
use App\Models\Campaign;
use Auth;

class CampaignController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->campaign = new Campaign;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Campaign::where('user_id', Auth::id() )->get();
        return view('account.common.campaigns.index', ['campaigns'=> $campaigns]);
    }



    public function create()
    {
        $pharmacies = Pharmacy::where('user_id', Auth::id())->get();
        return view('account.common.campaigns.create', ['campaign'=>$this->campaign, 'pharmacies'=>$pharmacies] );
    }

    public function store(Request $request){
//        dd($request);
        $this->campaign->fill($request->all());
        $this->campaign->status_id = 0;

        if ($request->hasFile('preview')) {
            $imageName = time().'.'.$request->preview->getClientOriginalExtension();
            $request->preview->move(public_path('uploads/campaigns/'), $imageName);
             $this->campaign->preview = $imageName;
        }

        $this->campaign->user_id = Auth::id();
        $this->campaign->show_amount = 0;
        $this->campaign->click_amount = 0;
        $this->campaign->save();
        return redirect('/admin/campaigns')->with('success', 'Новая кампания успешно добавлена');
    }

    public function aprove($id)
    {
        $campaign = Campaign::where('id',  '=', $id)->first();

        if($campaign->status_id == 0 or $campaign->status_id == 2 ) {
            $campaign->status_id = 1;
        } elseif($campaign->status_id == 1) {
            $campaign->status_id = 2;
        }
//        dd($campaign);
        $campaign->save();
        return redirect('admin/campaigns');
    }
}
