<?php

namespace App\Http\Controllers;
use Artisan;

use Illuminate\Http\Request;

class MigrationController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function run(Request $request)
    {
        Artisan::call('migrate:fresh');
    }        

    public function cache(Request $request)
    {
        Artisan::call('cache:clear');
    }    

    public function seed(Request $request)
    {
        Artisan::call('db:seed');
    }

}
