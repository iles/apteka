<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PriceList;

class PriceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('account.common.pricelists.index', ['data' => PriceList::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('account.common.pricelists.create', ['pharmacy_id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pricelist = new PriceList();

        $data = $this->validate($request, [
            'pharmacy_id'=> 'required',
            'drugs'=>'required',
            'drugs_info'=>'required',
        ]);


        $pricelist->savePriceList($data);
        return redirect('/pharmacies')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pricelist = PriceList::where('id',  '=', $id)->first();
        return view('account.common.pricelists.edit', compact('pricelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $pricelist = PriceList::where('id', '=', $request->id)->first();
        $pricelist->update($request->all());
        $pricelist->save();
        return redirect('/price/index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
