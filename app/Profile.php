<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = [
        'info', 'logo', 'is_online', 'firstname', 'lastname', 'patronic', 'phone',
    ];
    
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function saveProfile($data, $imageName)
    {
        $this->user_id = auth()->user()->id;
        $this->info = $data['info'];
        $this->logo = $imageName;
//        $this->is_online = $data['is_online'];
        $this->firstname = $data['firstname'];
        $this->lastname = $data['lastname'];
        $this->patronic = $data['patronic'];
        $this->phone = $data['phone'];

        $this->save();
        return 1;
    }
}
