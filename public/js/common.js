$(function() {

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};




	$(".popup-form").animated("bounceInDown", "fadeInDown"); 

		
	$(".accordeon dd").hide().prev().click(function() {
		$(this).parents(".accordeon").find("dd").not(this).slideUp().prev().removeClass("active");
		$(this).next().not(":visible").slideDown().prev().addClass("active");
	});


	$(".tab_item").not(":first").hide();
	$(".wrapper .tab").click(function() {
		$(".wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
		$(".tab_item").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("active");
	
	$('.popup-gallery').magnificPopup({
						delegate: 'a',
						type: 'image',
						tLoading: 'Загрузка изображения #%curr%...',
						mainClass: 'mfp-fade mfp-img-mobile',
						gallery: {
							enabled: true,
							navigateByImgClick: true,
							preload: [0,1] // Will preload 0 - before current, and 1 after the current image
						},
						image: {
							tError: '<a href="%url%">Изображение #%curr%</a> не загружено.',
							titleSrc: function(item) {
								return '';
							}
						}
					});

	$(".main_mnu_button").click(function() {
			$("nav > ul").slideToggle();
		});



	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
		mainClass: 'mfp-fade',
		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});



	$('.popup').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			fixedContentPos: true,
			mainClass: 'mfp-fade mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
			image: {
				verticalFit: true
			},
			zoom: {
				enabled: true,
				duration: 300 // don't foget to change the duration also in CSS
			}
		});


	//Каруселька
	//Документация: http://owlgraphic.com/owlcarousel/
	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    autoplay:true,
	    autoplayTimeout:6000,
	    animateOut: 'fadeOut',
	    pagination: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	})

	//Каруселька
	//Документация: http://owlgraphic.com/owlcarousel/
	$('.owl-carousel2').owlCarousel({
	    loop:true,
	    margin:10,
	    autoplay:true,
	    nav:true,
	    navText:['<span><i class="fa fa-angle-left" aria-hidden="true"></i></span>','<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>'],
	    autoplayTimeout:6000,
	    animateOut: 'fadeOut',
	    pagination: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	})

	//Каруселька
	//Документация: http://owlgraphic.com/owlcarousel/
	$('.owl-carousel3').owlCarousel({
	    loop:true,
	    margin:10,
	    autoplay:true,
	    nav:true,
	    navText:['<span><i class="fa fa-angle-left" aria-hidden="true"></i></span>','<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>'],
	    autoplayTimeout:6000,
	    animateOut: 'fadeOut',
	    pagination: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	})
	
	//Кнопка "Наверх"
	//Документация:
	//http://api.jquery.com/scrolltop/
	//http://api.jquery.com/animate/
	$("#top").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 800);
		return false;
	});

		$('.meds-examples a').on('click', function(e){
			e.preventDefault();
			var $element = $('.meds-select');
			var id = $(this).data('id');
			var text = $(this).html();
			var option = new Option(text, id, true, true);
			$element.append(option);
			$element.trigger('change');
		})


	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "assets/templates/mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(".done-w").fadeIn();
			setTimeout(function() {
				// Done Functions
				$(".done-w").fadeOut();
				$.magnificPopup.close();
				th.trigger("reset");
			}, 3000);
		});
		return false;
	});

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	//$("img, a").on("dragstart", function(event) { event.preventDefault(); });
	
});
function showBanner(box) {
    $.get('/banner/showad', function (data) {
        $(box).html(data);
    });
    setInterval(function getInterbalBanner() {
        $.get('/banner/showad', function (data) {
            $(box).html(data);
        });
    }, 20000)
}

	$(document).ready( function(){

		$('.meds-select').select2({
		    placeholder: "Введите название...",
		    minimumInputLength: 1,
			maximumSelectionLength: 5,
		    theme: "bootstrap",
		    ajax: {
		        url: '/meds',
		        dataType: 'json',
		        data: function (params) {
		            return {
		                q: $.trim(params.term)
		            };
		        },
		        processResults: function (data) {
		            return {
		                results: data
		            };
		        },
		        cache: true
		    }
		});
	} );



	function mapstuff(myCollection){


    ymaps.ready(['Map', 'Polygon']).then(function() {
        map = new ymaps.Map('map', {
            center: [43.2402, 76.9235],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'});

            myCollection = new ymaps.GeoObjectCollection();

 
            $.each(coords), function( index, value ) {
			 myCollection.add(new ymaps.Placemark([value]));
			};
            



            map.geoObjects.add(myCollection);

            map.setBounds(myCollection.getBounds());


            var drawButton = document.querySelector('#draw');

            drawButton.onclick = function() {
                drawButton.disabled = true;

                drawLineOverMap(map)
                    .then(function(coordinates) {
                        // Переводим координаты из 0..1 в географические.
                        var bounds = map.getBounds();
                        coordinates = coordinates.map(function(x) {
                            return [
                                // Широта (latitude).
                                // Y переворачивается, т.к. на canvas'е он направлен вниз.
                                bounds[0][0] + (1 - x[1]) * (bounds[1][0] - bounds[0][0]),
                                // Долгота (longitude).
                                bounds[0][1] + x[0] * (bounds[1][1] - bounds[0][1]),
                            ];
                        });

                        // Тут надо симплифицировать линию.
                        // Для простоты я оставляю только каждую третью координату.
                        coordinates = coordinates.filter(function (_, index) {
                            return index % 3 === 0;
                        });

                        // Удаляем старый полигон.
                        if (polygon) {
                            map.geoObjects.remove(polygon);
                        }

                        // Создаем новый полигон
                        polygon = new ymaps.Polygon([coordinates], {}, polygonOptions);

                        map.geoObjects.add(polygon);

                        searchPolygon();
                        drawButton.disabled = false;
                    });
            };
        });

        function drawLineOverMap(map) {
            var canvas = document.querySelector('#draw-canvas');
            var ctx2d = canvas.getContext('2d');
            var drawing = false;
            var coordinates = [];

            // Задаем размеры канвасу как у карты.
            var rect = map.container.getParentElement().getBoundingClientRect();
            canvas.style.width = rect.width + 'px';
            canvas.style.height = rect.height + 'px';
            canvas.width = rect.width;
            canvas.height = rect.height;

            // Применяем стили.
            ctx2d.strokeStyle = canvasOptions.strokeStyle;
            ctx2d.lineWidth = canvasOptions.lineWidth;
            canvas.style.opacity = canvasOptions.opacity;

            ctx2d.clearRect(0, 0, canvas.width, canvas.height);

            // Показываем канвас. Он будет сверху карты из-за position: absolute.
            canvas.style.display = 'block';

             $(canvas).on("mousedown", function(e){                
                drawing = true;
                coordinates =[];
                coordinates.push([e.offsetX, e.offsetY]);
             })             

            $(canvas).on("mousemove", function(e){
                if (drawing) {
                    var last = coordinates[coordinates.length - 1];
                    ctx2d.beginPath();
                    ctx2d.moveTo(last[0], last[1]);
                    ctx2d.lineTo(e.offsetX, e.offsetY);
                    ctx2d.stroke();
                    coordinates.push([e.offsetX, e.offsetY]);
                }
            })

            return new Promise(function(resolve) {
                // При отпускании мыши запоминаем координаты и скрываем канвас.
               $(canvas).on("mouseup", function(e){
    

                    coordinates.push([e.offsetX, e.offsetY]);
                    canvas.style.display = 'none';
                    drawing = false;

                    coordinates = coordinates.map(function(x) {
                        return [x[0] / canvas.width, x[1] / canvas.height];
                    });

                    resolve(coordinates);
                });  
            });
            

            

            // $(canvas).on("tapstart", function(e){            
            //     console.log('tap');
            //     console.log(e.originalEvent);
            //     drawing = true;
            //     console.log(e.changedTouches);
            //     coordinates =[];
            //     coordinates.push([e.offsetX, e.offsetY]);
            //  })
            
            // $(canvas).on("mousemove", function(e){
            //     if (drawing) {
            //         var last = coordinates[coordinates.length - 1];
            //         ctx2d.beginPath();
            //         ctx2d.moveTo(last[0], last[1]);
            //         ctx2d.lineTo(e.offsetX, e.offsetY);
            //         ctx2d.stroke();
            //         coordinates.push([e.offsetX, e.offsetY]);
            //     }
            // })                    


            //$(canvas).on('tapstart', function (e, touch) { console.log( 'tapstart', touch.offset ); })
             //$(canvas).on('tapmove', function (e, touch) { console.log( 'tapstart', touch.offset ); })


        

             // })



    }
	}
