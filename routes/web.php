<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Drug;



Route::get('/', 'HomeController@index');

Route::get('/test', function () {
    return view('test');
});


Route::post('/search', 'HomeController@find');
Route::get('/search', 'HomeController@index');


// // Маршруты аутентификации...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('/actions', 'HomeController@actions');
Route::get('/news', 'HomeController@news');

Route::post('/pharmacy/search', 'HomeController@find_pharmacy');
Route::get('/click/pharmacy/{id}', 'BannerController@ClickBanner');
Route::post('/comment/store}',  [ 'as' => 'comment.store', 'uses' => 'CommentController@store']);
Route::get('/banner/showad',  [ 'as' => 'add_show', 'uses' => 'BannerController@showad']);
Route::get('/search_polygon', 'HomeController@search_polygon');
Route::get('/findids', 'HomeController@find_ids');
//
//Route::get('/posts', 'PostController@index');
//Route::get('/post/{$id}', 'PostController@show');
//Route::get('/post/create', 'PostController@create');
//Route::get('/post/edit/{id}', 'PostController@edit');
//Route::post('/post/update/{id}', 'PostController@update');
//Route::post('/post/store', 'PostController@store');


Route::get('/posts', "HomeController@news");
Route::get('/article/{post}', "HomeController@newsShow");

Route::post('/post',"PostController@store");
Route::get('/posts/{post}/edit', "PostController@edit");
Route::patch('/posts/{post}', "PostController@update");
Route::get('/meds', "HomeController@meds");
Route::get('/medstop', "HomeController@medstop");

// // Маршруты регистрации...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Auth::routes();

Route::group(['middleware' => ['role:superadmin|admin']], function () {
 	Route::get('/dashboard', function ()    {
     	return view('account.admin.index');
       });
       Route::delete('/posts/{post}', [ 'as' => 'posts.destroy', 'uses' => 'PostController@destroy']);
   	Route::get('/dashboard/users', 'UserController@index');
    Route::get('/dashboard/pharmacies', 'PharmacyController@indexAdmin');
       
 });


Route::group(['middleware' => 'auth'], function () {

Route::get('/dashboard', 'PharmacyController@index');

Route::get('/admin', function ()    {
    return view('account.default.index');
});

Route::get('/dashboard/users', 'UserController@index');

Route::get('/pharmacy/aprove/{id}/{status}', 'PharmacyController@aprove');

Route::get('/campign/aprove/{id}', 'CampaignController@aprove');

Route::get('user/{name?}', function ($name = null) {
    return $name;
});

Route::get('/admin/pharmacies/{status?}', 'PharmacyController@index');

Route::get('/admin/pharmacies/{$id}', 'PharmacyController@show');

Route::get('/pharm/create', 'PharmacyController@create');

Route::get('/pharmacy/edit/{id}', 'PharmacyController@edit');
Route::post('/pharmacy/update/{id}', 'PharmacyController@update');
Route::post('/pharmacy/store/', 'PharmacyController@store');
Route::post('/pharmacy/destroy/{id}', 'PharmacyController@destroy');

Route::get('/price/index', 'PriceListController@index');
Route::get('/price/create/{id}', 'PriceListController@create');
Route::get('/price/edit/{id}', 'PriceListController@edit');
Route::post('/price/update', 'PriceListController@update');
Route::get('/admin/actions', 'ActionController@index');
Route::get('/actions/create', 'ActionController@create');    

Route::get('/admin/campaigns', 'CampaignController@index');
Route::get('/campaigns/create', 'CampaignController@create');
Route::post('/campaigns/store', [ 'as' => 'campaign.store', 'uses' => 'CampaignController@store']);

Route::get('/admin/banners/{id}', 'BannerController@index');
Route::get('/banners/create/{id}', 'BannerController@create');
Route::post('/banners/store', [ 'as' => 'banner.store', 'uses' => 'BannerController@store']);

//        Route::get('/profile/', 'UserController@profile');
//        Route::post('/profile/update/', 'UserController@profileUpdate');
Route::get('/profile', 'UserController@profile');
Route::get('/settings', 'UserController@settings');
Route::post('/profile/store', [ 'as' => 'profile.store', 'uses' => 'UserController@profileUpdate']);
Route::post('/user/store', [ 'as' => 'user.store', 'uses' => 'UserController@userUpdate']);

Route::post('/actions/store', 'ActionController@store');
Route::get('/actions/edit/{id}', 'ActionController@edit');
Route::post('/actions/destroy/{id}', 'ActionController@destroy');
        
Route::post('/actions/destroy', 'ActionController@destroy');

Route::post('/admin/post/destroy/{post}', 'PostController@destroy');
Route::get('/admin/posts/create',"PostController@create");
Route::get('/admin/posts',"PostController@index");

Route::post('/post',"PostController@store");
Route::get('/posts/{post}/edit', "PostController@edit");
Route::patch('/posts/{post}', "PostController@update");
});


Route::get('/run/84bde75961c', 'MigrationController@run');
Route::get('/seed/84bde75961c', 'MigrationController@seed');
Route::get('/cache/84bde75961c', 'MigrationController@cache');

Route::resource('pharmacy', 'PharmacyController');

Route::get('/best');

Route::get('/admin/users', [
    'middleware' => 'auth',
    'uses' => 'UserController@index'
]);


Route::get('/light', 'HomeController@light');
Route::get('/{slug}', 'HomeController@pharmacy');