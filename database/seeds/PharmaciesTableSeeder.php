<?php

use Illuminate\Database\Seeder;

class PharmaciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pharmacies')->delete();
        
        \DB::table('pharmacies')->insert(array (
            0 => 
            array (
                'user_id' => 2,
                'title' => 'Super Apteka',
                'slug' => 'super-apteka',
                'info' => 'Vse est',
                'logo' => '1538854369.jpg',
                'coordinates' => '43.2411, 76.9227',
                'address' => 'Джандосова 44',
                'phone' => '8(707)226 54 23',
                'time_work' => '09:00 - 23:00',
                'status_id' => 1,
                'is_online' => true,
                'rating' => 2,
                'created_at' => '2018-10-06 19:32:49',
                'updated_at' => '2018-10-06 19:32:49',
            ),       
            1 => 
            array (
                'user_id' => 2,
                'title' => 'Pharm',
                'slug' => 'pharm',
                'info' => 'Medicine',
                'logo' => '1538854369.jpg',
                'coordinates' => '43.2384, 76.9314',
                'address' => 'Манаса 55',
                'phone' => '8(707)226 54 23',
                'time_work' => NULL,
                'status_id' => 1,
                'is_online' => true,
                'rating' => 5,
                'created_at' => '2018-10-06 19:32:49',
                'updated_at' => '2018-10-06 19:32:49',
            ),       
            2 => 
            array (
                'user_id' => 2,
                'title' => 'Pharmacy',
                'slug' => 'pharmacy',
                'info' => 'Info Medicine',
                'logo' => '1538854369.jpg',
                'coordinates' => '43.2496, 76.9103',
                'address' => 'Джандосова 44',
                'phone' => '8(707)226 54 23',
                'time_work' => '10:00 - 15:00',
                'status_id' => 1,
                'is_online' => true,
                'rating' => 3,
                'created_at' => '2018-10-06 19:32:49',
                'updated_at' => '2018-10-06 19:32:49',
            ),
        ));

        factory(App\Models\Pharmacy::class, 45)->create();

        
    }
}
