<?php

use Illuminate\Database\Seeder;

class PriceListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('price_lists')->delete();
        
        \DB::table('price_lists')->insert(array (
            0 => 
            array (
                'pharmacy_id'=> 1,
                'drug_id'=> 1,
                'price'=> 500,
            ),
            1 => 
            array (
                'pharmacy_id'=> 1,
                'drug_id'=> 2,
                'price'=> 1500,
            ),            
            2 => 
            array (
                'pharmacy_id'=> 1,
                'drug_id'=> 3,
                'price'=> 1500,
            ),            
            3 => 
            array (
                'pharmacy_id'=> 1,
                'drug_id'=> 4,
                'price'=> 600,
            ),            
            4 => 
            array (
                'pharmacy_id'=> 1,
                'drug_id'=> 5,
                'price'=> 800,
            ),            
            5 => 
            array (
                'pharmacy_id'=> 2,
                'drug_id'=> 1,
                'price'=> 400,
            ),
            6 => 
            array (
                'pharmacy_id'=> 2,
                'drug_id'=> 2,
                'price'=> 1200,
            ),            
            7 => 
            array (
                'pharmacy_id'=> 2,
                'drug_id'=> 3,
                'price'=> 1700,
            ),            
            8 => 
            array (
                'pharmacy_id'=> 2,
                'drug_id'=> 4,
                'price'=> 400,
            ),            
            9 => 
            array (
                'pharmacy_id'=> 2,
                'drug_id'=> 5,
                'price'=> 8100,
            ), 

            10 => 
            array (
                'pharmacy_id'=> 3,
                'drug_id'=> 1,
                'price'=> 600,
            ),
            11 => 
            array (
                'pharmacy_id'=> 3,
                'drug_id'=> 2,
                'price'=> 1600,
            ),            
            12 => 
            array (
                'pharmacy_id'=> 3,
                'drug_id'=> 3,
                'price'=> 2100,
            ),            
            13 => 
            array (
                'pharmacy_id'=> 3,
                'drug_id'=> 4,
                'price'=> 200,
            ),            
            14 => 
            array (
                'pharmacy_id'=> 3,
                'drug_id'=> 5,
                'price'=> 2500,
            ),
        ));

        factory(App\Models\PriceList::class, 50)->create();
    }
}
