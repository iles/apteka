<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'create_pharmacy']);
        Permission::create(['name' => 'edit_pharmacy']);
        Permission::create(['name' => 'delete_pharmacy']);
        Permission::create(['name' => 'list_pharmacies']);
        Permission::create(['name' => 'news_managment']);
        Permission::create(['name' => 'actions_managment']);
        Permission::create(['name' => 'campaigns_managment']);
        Permission::create(['name' => 'campaigns_watch']);
        Permission::create(['name' => 'campaigns_create']);
        Permission::create(['name' => 'user_managment']);
        Permission::create(['name' => 'comments_managment']);
        Permission::create(['name' => 'pharmacy_control']);

        // create roles and assign created permissions


        $role = Role::create(['name' => 'superadmin']);
        $role->givePermissionTo(Permission::all());

        // or may be done by chaining
        $role = Role::create(['name' => 'moderator'])
            ->givePermissionTo(['create_pharmacy', 'edit_pharmacy', 'delete_pharmacy', 'list_pharmacies', 'news_managment', 'actions_managment', 'campaigns_managment', 'comments_managment', 'pharmacy_control' ]);        

        $role = Role::create(['name' => 'user'])
            ->givePermissionTo(['create_pharmacy', 'edit_pharmacy', 'campaigns_watch', 'campaigns_create' ]);
    }
}
