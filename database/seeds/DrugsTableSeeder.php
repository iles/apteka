<?php

use Illuminate\Database\Seeder;

class DrugsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('drugs')->delete();
        
        \DB::table('drugs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'drug_title' => 'аспирин',
                'symptoms' => 'головная боль, озноб',
            ),
            1 => 
            array (
                'id' => 2,
                'drug_title' => 'анальгин',
                'symptoms' => 'головная боль, озноб, температура',
            ),            
            2 => 
            array (
                'id' => 3,
                'drug_title' => 'звездочка',
                'symptoms' => 'ожог',
            ),            
            3 => 
            array (
                'id' => 4,
                'drug_title' => 'панадол',
                'symptoms' => 'жаропонижающее, обезболивающее',
            ),            
            4 => 
            array (
                'id' => 5,
                'drug_title' => 'цитрамон',
                'symptoms' => 'головная боль, давление',
            ),            
            5 => 
            array (
                'id' => 6,
                'drug_title' => 'валерианка',
                'symptoms' => 'головная боль, давление',
            ),            
            6 => 
            array (
                'id' => 7,
                'drug_title' => 'ношпа',
                'symptoms' => 'головная боль, давление',
            ),
        ));
    }
}
