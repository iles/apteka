<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PharmaciesTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(DrugsTableSeeder::class);
        $this->call(PriceListsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(BanerSeeder::class);
    }
}
