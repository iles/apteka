<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'name' => 'superadmin',
                'email' => 'superadmin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),
            1 => 
            array (
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$piJldAp.kLp5gjj8GZkpp.jGjYPhulISyTItxNLAJkvHazsgjLBve',
                'remember_token' => NULL,
                'created_at' => '2018-10-06 09:05:08',
                'updated_at' => '2018-10-06 09:05:08',
            ),            

            2 => 
            array (
                'name' => 'user1',
                'email' => 'user1@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),
        ));
        
        
    }
}
