<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(App\Models\PriceList::class, function (Faker $faker){
    return [
		    'pharmacy_id' => function () {
		      return factory(App\Models\Pharmacy::class)->create()->id;
		    },
            'drug_id'=> $faker->numberBetween($min = 1, $max = 7),
            'price'=> $faker->numberBetween($min = 300, $max = 1500)
    ];
});

