<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(App\Models\Banner::class, function (Faker $faker){
    return [
		    'campaign_id' => function () {
		      return factory(App\Models\Campaign::class)->create()->id;
		    },
		    'title' => $faker->company,
            'image'=> $faker->image($dir = public_path().'/uploads/banners/', $width = 165, $height = 200, null, false),
            'text'=> $faker->text($maxNbChars = 200),
            'pharmacy_id'=> $faker->numberBetween($min = 1, $max = 48),
    ];
});
