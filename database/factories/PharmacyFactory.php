<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Pharmacy::class, function (Faker $faker) {
    $title = $faker->company;
    return [
        'user_id' => 2,
        'title' => $title,
        'slug' => Str::slug($title, '-'),
        'info' => $faker->text($maxNbChars = 200),
        'logo' => $faker->image($dir = public_path().'/uploads/logos/', $width = 200, $height = 278, null, false),
        'address' => $faker->address,
        'phone' => $faker->tollFreePhoneNumber,
        'time_work' => '09:00 - 12:00',
        'coordinates' => $faker->latitude($min = 43.2226, $max = 43.2660).', '.$faker->longitude($min = 76.8713, $max = 76.9623),
        'status_id' => 1,
        'rating' => $faker->numberBetween($min = 1, $max = 5),
        'is_online' => 1,
    ];
});
