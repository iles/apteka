<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(App\Models\Campaign::class, function (Faker $faker){
    return [
		    'status_id' => 1,
		    'user_id' => 2,
            'pharmacy_id'=> $faker->numberBetween($min = 1, $max = 48),
		    'title' => $faker->company,
		    'type' => 1,
		    'date_start' => $faker->dateTime($max = 'now', $timezone = null),
		    'date_end' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});