<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status_id');
            $table->integer('user_id');
            $table->integer('pharmacy_id');
            $table->string('title');
            $table->integer('type');
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->integer('budget')->nullable();
            $table->integer('rest')->nullable()->unsigned();
            $table->tinyInteger('payment_status_id')->nullable()->unsigned();
            $table->integer('show_amount')->nullable();
            $table->integer('click_amount')->nullable();
            $table->string('preview')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
